<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Ui\Cli;

use Doctrine\DBAL\Connection;
use Grifix\App\Security\Application\User\Commands\ChangeEmail\ChangeUserEmailCommand;
use Grifix\App\Shared\Application\CommandBusInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class UpdateUsersCommand extends Command
{

    public const NAME = 'grifix:app:shared:update-users';

    public const ARG_DELAY = 'delay';

    protected static $defaultName = self::NAME;

    public function __construct(
        private readonly CommandBusInterface $commandBus,
        private readonly Connection $connection)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(self::ARG_DELAY, InputArgument::OPTIONAL, 'delay between updates', 1);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        while (true) {
            try {
                $row = $this->connection->fetchAssociative('select * from grifix_app_security.users order by random() limit 1');
                $this->commandBus->executeCommand(new ChangeUserEmailCommand($row['id'], uniqid()));
                //sleep((int)$input->getArgument(self::ARG_DELAY));
            } catch (\Throwable $e) {
                $output->writeln($e->getMessage());
            }

        }
        return self::SUCCESS;
    }
}
