<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Ui\Cli;

use Grifix\App\Security\Application\User\Commands\Create\CreateUserCommand;
use Grifix\App\Shared\Application\CommandBusInterface;
use Grifix\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateUsersCommand extends Command
{

    public const NAME = 'grifix:app:shared:create-users';

    public const ARG_QUANTITY = 'quantity';

    protected static $defaultName = self::NAME;

    public function __construct(private readonly CommandBusInterface $commandBus)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->addArgument(self::ARG_QUANTITY, InputArgument::OPTIONAL, 'quantity', 100);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        for ($i = 0; $i < (int) $input->getArgument(self::ARG_QUANTITY); $i++) {
            $this->commandBus->executeCommand(new CreateUserCommand(Uuid::createRandom(), 'email_' . $i+1));
        }
        return self::SUCCESS;
    }
}
