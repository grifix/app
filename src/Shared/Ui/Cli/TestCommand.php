<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Ui\Cli;

use Grifix\App\Security\Application\User\Commands\Create\CreateUserCommand;
use Grifix\App\Shared\Application\CommandBusInterface;
use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\Repository\EventRepositoryInterface;
use Grifix\EventStore\EventStoreInterface;
use Grifix\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class TestCommand extends Command
{

    public const NAME = 'test';

    protected static $defaultName = self::NAME;

    public function __construct(private readonly EventRepositoryInterface $eventRepository)
    {
        parent::__construct();
    }

    protected function configure(): void
    {

    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $arr = $this->eventRepository->find(
            fromNumber: BigInt::create(10),
            streamId: '698fb54f-7451-4fe5-afb8-e6daab08da52',
            chunkSize: 10
        );
        return self::SUCCESS;
    }
}
