<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Ui\Listeners;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Event\ConsoleErrorEvent;

final class ConsoleErrorListener
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function onConsoleError(ConsoleErrorEvent $event): void
    {
        $command = $event->getCommand();
        $exception = $event->getError();

        $message = sprintf(
            '%s: %s (uncaught exception) at %s line %s while running console command `%s`',
            get_class($exception),
            $exception->getMessage(),
            $exception->getFile(),
            $exception->getLine(),
            $command?->getName()
        );

        $this->logger->error($message, array('exception' => $exception));
    }
}
