<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Application;

interface CommandBusInterface
{
    public function executeCommand(object $command):void;
}
