<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Infrastructure\Application;

use Doctrine\ORM\EntityManagerInterface;
use Grifix\App\Shared\Application\CommandBusInterface;
use Grifix\EventStore\EventStoreInterface;
use Symfony\Component\Messenger\Exception\HandlerFailedException;
use Symfony\Component\Messenger\MessageBusInterface;

final class CommandBus implements CommandBusInterface
{

    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly EventStoreInterface $eventStore,
        private readonly MessageBusInterface $messageBus
    )
    {
    }

    public function executeCommand(object $command): void
    {
        $this->entityManager->beginTransaction();
        try {
            $this->messageBus->dispatch($command);
        } catch (HandlerFailedException $exception) {
            throw $exception->getPrevious();
        } finally {
            $this->entityManager->flush();
            $this->eventStore->flush();
            try {
                $this->entityManager->commit();
            } catch (\Throwable $exception) {
                $this->entityManager->rollback();
                throw $exception;
            }
        }
    }
}
