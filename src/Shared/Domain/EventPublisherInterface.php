<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Domain;

use Grifix\Uuid\Uuid;

interface EventPublisherInterface
{
    public function publishEvent(object $event, string $streamType, Uuid $streamId): void;
}
