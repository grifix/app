<?php
declare(strict_types=1);

namespace Grifix\App\Shared\Domain;

use Grifix\Uuid\Uuid;

/**
 * @property Uuid $id
 * @property EventPublisherInterface $outside
 */
trait AggregateRootTrait
{
    protected function publishEvent(object $event):void{
        $this->outside->publishEvent($event, get_class($this), $this->id);
    }
}
