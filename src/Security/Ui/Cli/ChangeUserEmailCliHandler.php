<?php
declare(strict_types=1);

namespace Grifix\App\Security\Ui\Cli;

use Grifix\App\Security\Application\User\Commands\ChangeEmail\ChangeUserEmailCommand;
use Grifix\App\Security\Application\User\Commands\Create\CreateUserCommand;
use Grifix\App\Shared\Application\CommandBusInterface;
use Grifix\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class ChangeUserEmailCliHandler extends Command
{
    public const NAME = 'app:security:change-user-email';
    public const ARG_EMAIL = 'email';
    public const ARG_USER_ID = 'user_id';

    public function __construct(private readonly CommandBusInterface $commandBus)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName(self::NAME);
        $this->addArgument(self::ARG_USER_ID, InputArgument::REQUIRED, 'user id');
        $this->addArgument(self::ARG_EMAIL, InputArgument::REQUIRED, 'email');
        $this->setDescription('Changes user eamil');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int{
        $this->commandBus->executeCommand(new ChangeUserEmailCommand(
            $input->getArgument(self::ARG_USER_ID),
            $input->getArgument(self::ARG_EMAIL)
        ));
        return self::SUCCESS;
    }
}
