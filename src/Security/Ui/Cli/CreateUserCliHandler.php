<?php
declare(strict_types=1);

namespace Grifix\App\Security\Ui\Cli;

use Grifix\App\Security\Application\User\Commands\Create\CreateUserCommand;
use Grifix\App\Shared\Application\CommandBusInterface;
use Grifix\Uuid\Uuid;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class CreateUserCliHandler extends Command
{
    public const NAME = 'app:security:create-user';
    public const ARG_EMAIL = 'email';

    public function __construct(private readonly CommandBusInterface $commandBus)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName(self::NAME);
        $this->addArgument(self::ARG_EMAIL, InputArgument::REQUIRED, 'email');
        $this->setDescription('Creates a user');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int{
        $id = Uuid::createRandom();
        $this->commandBus->executeCommand(new CreateUserCommand(
            $id,
            $input->getArgument(self::ARG_EMAIL)
        ));
        $output->writeln(sprintf('User with id [%s] has been created', $id));
        return self::SUCCESS;
    }
}
