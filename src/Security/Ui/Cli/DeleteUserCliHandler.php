<?php
declare(strict_types=1);

namespace Grifix\App\Security\Ui\Cli;

use Grifix\App\Security\Application\User\Commands\Delete\DeleteUserCommand;
use Grifix\App\Shared\Application\CommandBusInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class DeleteUserCliHandler extends Command
{
    public const NAME = 'app:security:delete-user';
    public const ARG_USER_ID = 'user_id';

    public function __construct(private readonly CommandBusInterface $commandBus)
    {
        parent::__construct();
    }

    protected function configure()
    {
        $this->setName(self::NAME);
        $this->addArgument(self::ARG_USER_ID, InputArgument::REQUIRED, 'user id');
        $this->setDescription('Deletes an user');
        parent::configure();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->commandBus->executeCommand(new DeleteUserCommand(
            $input->getArgument(self::ARG_USER_ID),
        ));
        return self::SUCCESS;
    }
}
