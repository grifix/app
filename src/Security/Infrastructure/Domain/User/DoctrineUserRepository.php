<?php
declare(strict_types=1);

namespace Grifix\App\Security\Infrastructure\Domain\User;

use Grifix\App\Security\Domain\User\Repository\Exceptions\UserDoesNotExistException;
use Grifix\App\Security\Domain\User\Repository\UserRepositoryInterface;
use Grifix\App\Security\Domain\User\User;
use Grifix\App\Security\Infrastructure\Common\Doctrine\Entity\UserEntity;
use Grifix\Entity\AbstractRepository;

final class DoctrineUserRepository extends AbstractRepository implements UserRepositoryInterface
{
    public function get(string $id): User
    {
        /** @var User|null $result */
        $result = $this->doFind($id);
        if (null === $result) {
            throw new UserDoesNotExistException($id);
        }
        return $result;
    }

    public function add(User $user): void
    {
        $this->doAdd($user);
    }

    public function update(User $user): void
    {
        $this->doUpdate($user);
    }

    protected function getEntityClass(): string
    {
        return UserEntity::class;
    }
}
