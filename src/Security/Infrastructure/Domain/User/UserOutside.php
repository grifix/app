<?php
declare(strict_types=1);

namespace Grifix\App\Security\Infrastructure\Domain\User;

use Grifix\App\Security\Domain\User\UserOutsideInterface;
use Grifix\Clock\ClockInterface;
use Grifix\EventStore\EventStoreInterface;
use Grifix\Uuid\Uuid;

final class UserOutside implements UserOutsideInterface
{

    public function __construct(
        private readonly EventStoreInterface $eventStore,
        private readonly ClockInterface $clock
    )
    {
    }

    public function publishEvent(object $event, string $streamType, Uuid $streamId): void
    {
        $this->eventStore->storeEvent($event, $streamType, $streamId);
    }

    public function getCurrentDate(): \DateTimeImmutable
    {
        return $this->clock->getCurrentTime();
    }
}
