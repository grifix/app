<?php
declare(strict_types=1);

namespace Grifix\App\Security\Infrastructure\Application\User\Projections;

use Doctrine\DBAL\Connection;
use Grifix\App\Security\Application\User\Projections\UserProjection\UserDto;
use Grifix\App\Security\Application\User\Projections\UserProjection\UserProjectionInterface;

final class DbalUserProjection implements UserProjectionInterface
{

    private const TABLE = 'grifix_app_security.user_projections';

    public function __construct(private readonly Connection $connection)
    {
    }

    public function add(UserDto $user): void
    {
        $this->connection->insert(
            self::TABLE,
            [
                'id' => $user->id,
                'email' => $user->email
            ]
        );
    }

    public function delete(string $userId): void
    {
        $this->connection->delete(self::TABLE, ['id' => $userId]);
    }

    public function changeEmail(string $userId, string $newEmail, int $counter): void
    {
        $this->connection->update(
            self::TABLE,
            ['email' => $newEmail],
            ['id' => $userId]
        );

        $this->connection->insert(
            'grifix_app_security.user_updates_projection',
            [
                'user_id' => $userId,
                'counter' => $counter,
                'email' => $newEmail,
                'date' => (new \DateTimeImmutable())->format('Y-m-d H:i:s.u')
            ]
        );
    }
}
