<?php

declare(strict_types=1);

namespace Grifix\App\Security\Infrastructure\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220506193256 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $sql = <<<SQL
        create table grifix_app_security.user_updates_projection
(
    user_id uuid      not null,
    counter bigint    not null,
    email text not null,
    date    timestamp not null
);
SQL;
        $this->addSql($sql);


    }

    public function down(Schema $schema): void
    {
        $this->addSql('delete table grifix_app_security.user_updates_projection');
    }
}
