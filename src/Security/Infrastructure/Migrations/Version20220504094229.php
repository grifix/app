<?php

declare(strict_types=1);

namespace Grifix\App\Security\Infrastructure\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220504094229 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $sql = <<<SQL
create table grifix_app_security.user_projections
(
    id    uuid
        constraint user_projections_pk
            primary key,
    email varchar(255) not null
);

SQL;
        $this->addSql($sql);


    }

    public function down(Schema $schema): void
    {
        $this->addSql('drop table grifix_app_security.user_projections');
    }
}
