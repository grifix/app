<?php

declare(strict_types=1);

namespace Grifix\App\Security\Infrastructure\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220423122309 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->createSchema();
        $this->createUsersTable();

    }

    public function down(Schema $schema): void
    {
        $this->dropUsersTable();
        $this->dropSchema();

    }

    private function createSchema(): void
    {
        $this->addSql('create schema grifix_app_security');
    }

    private function dropSchema(): void
    {
        $this->addSql('drop schema grifix_app_security');
    }

    public function createUsersTable(): void
    {
        $sql = <<<SQL
create table grifix_app_security.users
(
    id      uuid   not null
        constraint users_pk
            primary key,
    data    jsonb  not null
);


SQL;
        $this->addSql($sql);
    }

    public function dropUsersTable(): void
    {
        $this->addSql('drop table grifix_app_security.users');
    }
}
