<?php
declare(strict_types=1);

namespace Grifix\App\Security\Infrastructure\Common\Doctrine\Entity;

use Grifix\Entity\AbstractEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="Grifix\App\Security\Infrastructure\Domain\User\DoctrineUserRepository")
 * @ORM\Table(name="grifix_app_security.users")
 */
final class UserEntity extends AbstractEntity
{

}
