<?php
declare(strict_types=1);

namespace Grifix\App\Security\Domain\User\Events;

final class UserDeletedEvent
{
    public function __construct(public readonly string $userId, public readonly string $dateOfDeletion)
    {
    }
}
