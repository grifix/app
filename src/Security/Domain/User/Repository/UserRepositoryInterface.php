<?php
declare(strict_types=1);

namespace Grifix\App\Security\Domain\User\Repository;

use Grifix\App\Security\Domain\User\Repository\Exceptions\UserDoesNotExistException;
use Grifix\App\Security\Domain\User\User;

interface UserRepositoryInterface
{
    /**
     * @throws UserDoesNotExistException
     */
    public function get(string $id): User;

    public function add(User $user): void;

    public function update(User $user): void;

}
