<?php
declare(strict_types=1);

namespace Grifix\App\Security\Domain\User\Repository\Exceptions;

final class UserDoesNotExistException extends \Exception
{

    public function __construct(public readonly string $userId)
    {
        parent::__construct(sprintf('User with id [%s] does not exist!', $userId));
    }
}
