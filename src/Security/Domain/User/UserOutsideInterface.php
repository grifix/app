<?php
declare(strict_types=1);

namespace Grifix\App\Security\Domain\User;

use Grifix\App\Shared\Domain\EventPublisherInterface;

interface UserOutsideInterface extends EventPublisherInterface
{
    public function getCurrentDate():\DateTimeImmutable;
}
