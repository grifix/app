<?php
declare(strict_types=1);

namespace Grifix\App\Security\Domain\User;

use Grifix\App\Security\Domain\User\Events\UserCreatedEvent;
use Grifix\App\Security\Domain\User\Events\UserDeletedEvent;
use Grifix\App\Security\Domain\User\Events\UserEmailChangedEvent;
use Grifix\App\Shared\Domain\AggregateRootTrait;
use Grifix\Uuid\Uuid;

final class User
{
    use AggregateRootTrait;

    private ?\DateTimeImmutable $dateOfDeletion = null;

    private int $counter = 0;

    public function __construct(
        private        readonly UserOutsideInterface $outside,
        private        readonly Uuid $id,
        private string $email
    )
    {
        $this->publishEvent(
            new UserCreatedEvent(
                (string)$id,
                $email
            )
        );
    }

    public function changeEmail(string $newEmail): void
    {
        $this->email = $newEmail;
        $this->counter++;
        $this->publishEvent(
            new UserEmailChangedEvent(
                (string)$this->id,
                $newEmail,
                $this->counter
            )
        );
    }

    public function delete(): void
    {
        $this->dateOfDeletion = $this->outside->getCurrentDate();
        $this->publishEvent(
            new UserDeletedEvent(
                (string) $this->id,
                $this->dateOfDeletion->format(\DateTimeInterface::ATOM)
            )
        );
    }
}
