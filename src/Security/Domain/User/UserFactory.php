<?php
declare(strict_types=1);

namespace Grifix\App\Security\Domain\User;

use Grifix\Uuid\Uuid;

final class UserFactory
{


    public function __construct(private readonly UserOutsideInterface $outside)
    {
    }

    public function createUser(Uuid $id, string $email): User
    {
        return new User($this->outside, $id, $email);
    }
}
