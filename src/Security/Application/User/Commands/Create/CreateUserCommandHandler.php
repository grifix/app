<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Commands\Create;

use Grifix\App\Security\Domain\User\Repository\UserRepositoryInterface;
use Grifix\App\Security\Domain\User\UserFactory;

final class CreateUserCommandHandler
{

    public function __construct(
        private readonly UserRepositoryInterface $userRepository,
        private readonly UserFactory $userFactory
    )
    {
    }

    public function __invoke(CreateUserCommand $command): void
    {
        $this->userRepository->add($this->userFactory->createUser($command->id, $command->email));
    }
}
