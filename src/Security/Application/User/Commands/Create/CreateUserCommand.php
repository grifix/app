<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Commands\Create;

use Grifix\Uuid\Uuid;

final class CreateUserCommand
{
    public function __construct(
        public readonly Uuid $id,
        public readonly string $email
    )
    {
    }
}
