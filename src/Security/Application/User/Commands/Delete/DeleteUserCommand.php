<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Commands\Delete;

final class DeleteUserCommand
{

    public function __construct(public readonly string $userId)
    {
    }
}
