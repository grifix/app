<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Commands\Delete;

use Grifix\App\Security\Domain\User\Repository\UserRepositoryInterface;

final class DeleteUserCommandHandler
{
    public function __construct(private readonly UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(DeleteUserCommand $command): void
    {
        $user = $this->userRepository->get($command->userId);
        $user->delete();
        $this->userRepository->update($user);
    }
}
