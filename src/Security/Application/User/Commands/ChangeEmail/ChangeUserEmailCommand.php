<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Commands\ChangeEmail;

final class ChangeUserEmailCommand
{

    public function __construct(
        public readonly string $userId,
        public readonly string $newEmail
    )
    {
    }
}
