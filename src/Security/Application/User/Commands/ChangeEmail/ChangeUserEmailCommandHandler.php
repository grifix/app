<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Commands\ChangeEmail;

use Grifix\App\Security\Domain\User\Repository\UserRepositoryInterface;

final class ChangeUserEmailCommandHandler
{
    public function __construct(private readonly UserRepositoryInterface $userRepository)
    {
    }

    public function __invoke(ChangeUserEmailCommand $command):void
    {
        $user = $this->userRepository->get($command->userId);
        $i=0;
        for($i=0; $i<1000; $i++){
            $user->changeEmail($command->newEmail.'_'.$i);
            $this->userRepository->update($user);
        }
    }
}
