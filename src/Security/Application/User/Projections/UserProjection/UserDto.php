<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Projections\UserProjection;

final class UserDto
{

    public function __construct(
        public readonly string $id,
        public readonly string $email
    )
    {
    }
}
