<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Projections\UserProjection;

interface UserProjectionInterface
{
    public function add(UserDto $user):void;

    public function changeEmail(string $userId, string $newEmail, int $counter): void;

    public function delete(string $userId):void;
}
