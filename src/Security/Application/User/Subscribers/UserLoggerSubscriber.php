<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Subscribers;

use Grifix\App\Security\Domain\User\Events\UserCreatedEvent;
use Grifix\App\Security\Domain\User\Events\UserEmailChangedEvent;
use Psr\Log\LoggerInterface;

final class UserLoggerSubscriber
{
    public function __construct(private readonly LoggerInterface $logger)
    {
    }

    public function onUserCreated(UserCreatedEvent $event): void
    {
        $this->logger->debug('User created', ['userId' => $event->userId]);
    }

    public function onUserEmailChanged(UserEmailChangedEvent $event): void
    {
        $this->logger->debug('User email changed', ['userId' => $event->userId, 'newEmail' => $event->newEmail]);
    }
}
