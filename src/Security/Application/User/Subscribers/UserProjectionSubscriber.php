<?php
declare(strict_types=1);

namespace Grifix\App\Security\Application\User\Subscribers;

use Grifix\App\Security\Application\User\Projections\UserProjection\UserDto;
use Grifix\App\Security\Application\User\Projections\UserProjection\UserProjectionInterface;
use Grifix\App\Security\Domain\User\Events\UserCreatedEvent;
use Grifix\App\Security\Domain\User\Events\UserDeletedEvent;
use Grifix\App\Security\Domain\User\Events\UserEmailChangedEvent;

final class UserProjectionSubscriber
{

    public function __construct(private readonly UserProjectionInterface $projection)
    {
    }

    public function onUserCreated(UserCreatedEvent $event): void
    {
        $this->projection->add(new UserDto($event->userId, $event->email));
    }

    public function onUserEmailChanged(UserEmailChangedEvent $event): void
    {
        $this->projection->changeEmail($event->userId, $event->newEmail, $event->counter);
    }

    public function onUserDeleted(UserDeletedEvent $event): void
    {
        $this->projection->delete($event->userId);
    }
}
