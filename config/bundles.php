<?php

return [
    Symfony\Bundle\FrameworkBundle\FrameworkBundle::class => ['all' => true],
    Doctrine\Bundle\DoctrineBundle\DoctrineBundle::class => ['all' => true],
    Doctrine\Bundle\MigrationsBundle\DoctrineMigrationsBundle::class => ['all' => true],
    Grifix\EventStoreBundle\GrifixEventStoreBundle::class => ['all' => true],
    Grifix\ClockBundle\GrifixClockBundle::class => ['all' => true],
    Grifix\EntityBundle\GrifixEntityBundle::class => ['all' => true],
    Grifix\NormalizerBundle\GrifixNormalizerBundle::class => ['all' => true],
    Grifix\ProcessManagerBundle\GrifixProcessManagerBundle::class => ['all' => true],
    Symfony\Bundle\MonologBundle\MonologBundle::class => ['all' => true],
    Grifix\WorkerBundle\GrifixWorkerBundle::class => ['all' => true],
    Grifix\MemoryBundle\GrifixMemoryBundle::class => ['all' => true],
];
