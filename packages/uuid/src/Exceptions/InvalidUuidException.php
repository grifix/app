<?php
declare(strict_types=1);

namespace Grifix\Uuid\Exceptions;

final class InvalidUuidException extends \Exception
{
    public function __construct(public readonly string $uuid)
    {
        parent::__construct(sprintf('Uuid [%s] is invalid!', $uuid));
    }
}
