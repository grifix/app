<?php
declare(strict_types=1);

namespace Grifix\Reflection;

use ReflectionObject as BaseReflectionObject;

final class ReflectionObject
{

    private BaseReflectionObject $reflectionObject;

    public function __construct(private object $object)
    {
        $this->reflectionObject = new BaseReflectionObject($object);
    }

    public function getPropertyValue(string $propertyPath): mixed
    {
        $objectAndProperty = $this->getNestedObjectAndPropertyName($this->object, $propertyPath);
        return $this->doGetPropertyValue($objectAndProperty[0], $objectAndProperty[1]);
    }

    public function setPropertyValue(string $propertyPath, mixed $value): void
    {
        $objectAndProperty = $this->getNestedObjectAndPropertyName($this->object, $propertyPath);
        $this->doSetPropertyValue($objectAndProperty[0], $objectAndProperty[1], $value);
    }

    private function getNestedObjectAndPropertyName($object, string $propertyPath): array
    {
        if (!str_contains($propertyPath, '.')) {
            return [$object, $propertyPath];
        }
        $propertyNameArray = explode('.', $propertyPath);
        $childPropertyName = array_shift($propertyNameArray);
        $childObject = $this->doGetPropertyValue($object, $childPropertyName);
        return $this->getNestedObjectAndPropertyName($childObject, implode('.', $propertyNameArray));
    }

    private function doGetPropertyValue($object, string $propertyName)
    {
        $reflectionObject = $this->reflectionObject;
        if ($object !== $this->object) {
            $reflectionObject = new BaseReflectionObject($object);
        }
        $property = $reflectionObject->getProperty($propertyName);
        $property->setAccessible(true);
        $result = $property->getValue($object);
        $property->setAccessible(false);
        return $result;
    }

    private function doSetPropertyValue($object, string $propertyName, $value): void
    {
        $reflection = $this->reflectionObject;
        if ($object !== $this->object) {
            $reflection = new BaseReflectionObject($object);
        }
        $property = $reflection->getProperty($propertyName);
        $property->setAccessible(true);
        $property->setValue($object, $value);
        $property->setAccessible(false);
    }

    public function getObject(): object
    {
        return $this->object;
    }


    public function getPropertyValues(): array
    {
        $result = [];
        foreach ($this->reflectionObject->getProperties() as $property) {
            $result[$property->getName()] = $this->getPropertyValue($property->getName());
        }
        return $result;
    }
}
