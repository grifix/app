<?php
declare(strict_types=1);

namespace Grifix\Reflection\Tests\Stub;

final class Engine
{

    public function __construct(public int $volume)
    {
    }
}
