<?php
declare(strict_types=1);

namespace Grifix\Reflection\Tests;

use Grifix\Reflection\ReflectionObject;
use Grifix\Reflection\Tests\Stub\Car;
use Grifix\Reflection\Tests\Stub\Engine;
use PHPUnit\Framework\TestCase;

final class ReflectingObjectTest extends TestCase
{
    public function testItGetsPropertyValue(): void
    {
        $car = new Car('Mercedes', new Engine(5));
        $reflection = new ReflectionObject($car);
        self::assertEquals($car->producer, $reflection->getPropertyValue('producer'));
        self::assertEquals($car->engine->volume, $reflection->getPropertyValue('engine.volume'));
    }

    public function testItSetsPropertyValue(): void
    {
        $car = new Car('Mercedes', new Engine(5));
        $newEngineVolume = 10;
        $newCarProducer = 'Volvo';
        $reflection = new ReflectionObject($car);
        $reflection->setPropertyValue('producer', $newCarProducer);
        $reflection->setPropertyValue('engine.volume', $newEngineVolume);
        self::assertEquals($newCarProducer, $car->producer);
        self::assertEquals($newEngineVolume, $car->engine->volume);
    }
}
