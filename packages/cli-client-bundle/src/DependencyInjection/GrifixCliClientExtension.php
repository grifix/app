<?php
declare(strict_types=1);

namespace Grifix\CliClientBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Extension\PrependExtensionInterface;

final class GrifixCliClientExtension extends Extension implements PrependExtensionInterface
{

}
