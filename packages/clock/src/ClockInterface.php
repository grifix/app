<?php
declare(strict_types=1);

namespace Grifix\Clock;

interface ClockInterface
{
    public const SECOND = 1e-6;
    public const MINUTE = 60;
    public const HOUR = self::MINUTE * 60;
    public const DAY = self::HOUR * 24;

    public function getCurrentTime(): \DateTimeImmutable;
}
