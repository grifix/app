<?php
declare(strict_types=1);

namespace Grifix\Clock;

final class SystemClock implements ClockInterface
{

    public function getCurrentTime(): \DateTimeImmutable
    {
        return new \DateTimeImmutable();
    }
}
