<?php
declare(strict_types=1);

namespace Grifix\Clock;

use DateTimeImmutable;

final class FrozenClock implements ClockInterface
{
    private ?DateTimeImmutable $frozenTime = null;

    public function __construct()
    {
    }

    public function getCurrentTime(): \DateTimeImmutable
    {
        if ($this->frozenTime) {
            return $this->frozenTime;
        }
        return new \DateTimeImmutable();
    }

    public function freezeTime(DateTimeImmutable $date): void
    {
        $this->frozenTime = $date;
    }

    public function unfreezeTime(): void
    {
        $this->frozenTime = null;
    }
}
