Usage:

```php
$wrapped = \Grifix\ArrayWrapper\ArrayWrapper::create([
    'name' => 'Joe',
    'address' => [
        'city' => 'London'
    ],
    'tasks' => [
        'wash car',
        'buy food'
    ]      
])

$wrapped->getElement('address');// returns ['city' => 'London']
$wrapped->getElement('address.city'); // 'London' 
$wrapped->getElement('tasks.1'); //buy food
$wrapped->hasElement('tasks.1'); //true
$wrapped->hasElement('tasks.3'); //false
$wrapped->isAssociative(); //true
$wrapped->setElement('address.country', 'Uk');
$wrapped->addElement('tasks', 'clean the apartment');
$wrapped->getFistElement(); //returns "Joe"
$wrapped->getLastElement(); //returns tasks array


```
