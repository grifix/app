<?php
declare(strict_types=1);

namespace Grifix\ArrayWrapper\Tests;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\ArrayWrapper\Exceptions\ElementIsNotAnArrayException;
use PHPUnit\Framework\TestCase;

final class ArrayWrapperTest extends TestCase
{
    /**
     * @dataProvider itGetsElementDataProvider
     */
    public function testItGetsElement(array $array, string|int $pathKey, mixed $expectedResult, mixed $defaultValue = null): void
    {
        $wrapper = ArrayWrapper::create($array);
        self::assertEquals($expectedResult, $wrapper->getElement($pathKey, $defaultValue));
    }

    public function itGetsElementDataProvider(): array
    {
        return [
            [
                [0 => 'foo'],
                0,
                'foo'
            ],
            [
                ['name' => 'Mike'],
                'name',
                'Mike'
            ],
            [
                [
                    'user' => [
                        'name' => 'Mike'
                    ]
                ],
                'user.name',
                'Mike'
            ],
            [
                [
                    'user' => [
                        'names' => [
                            0 => 'Mike'
                        ]
                    ]
                ],
                'user.names.0',
                'Mike'
            ],
            [
                [],
                1,
                null
            ],
            [
                [],
                1,
                'test',
                'test'
            ]
        ];
    }

    /**
     * @dataProvider itSetsElementDataProvider
     */
    public function testItSetsElement(string|int $pathKey, mixed $value, array $expectedResult)
    {
        $wrapper = ArrayWrapper::create();
        $wrapper->setElement($pathKey, $value);
        self::assertEquals($expectedResult, $wrapper->getWrapped());
    }

    public function itSetsElementDataProvider(): array
    {
        return [
            [
                0,
                'Mike',
                ['Mike']
            ],
            [
                'name',
                'Mike',
                [
                    'name' => 'Mike'
                ]
            ],
            [
                'user.name',
                'Mike',
                [
                    'user' => [
                        'name' => 'Mike'
                    ]
                ]
            ],
            [
                'user.names.0',
                'Mike',
                [
                    'user' => [
                        'names' => [
                            0 => 'Mike'
                        ]
                    ]
                ]
            ]
        ];
    }

    /**
     * @dataProvider itHasElementDataProvider
     */
    public function testItHasElement(array $array, string|int $pathKey, bool $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($array);
        self::assertEquals($expectedResult, $wrapper->hasElement($pathKey));
    }

    public function itHasElementDataProvider(): array
    {
        return [
            [
                [],
                0,
                false
            ],
            [
                [0 => null],
                0,
                true
            ],
            [
                [0 => 'value'],
                0,
                true
            ],
            [
                ['user' => 'Mike'],
                'user',
                true
            ],
            [
                ['user' => 'Mike'],
                'user.name',
                false
            ],
            [
                [
                    'user' => [
                        'name' => 'Mike'
                    ]
                ],
                'user.name',
                true
            ],
            [
                [
                    'user' => [
                        'name' => null
                    ]
                ],
                'user.name',
                true
            ],
            [
                [

                    'user' => [
                        'names' => []
                    ]

                ],
                'user.names',
                true
            ],
            [
                [

                    'user' => [
                        'names' => []
                    ]

                ],
                'user.names.0',
                false
            ],
            [
                [

                    'user' => [
                        'names' => ['Mike']
                    ]

                ],
                'user.names.0',
                true
            ],

        ];
    }

    /**
     * @dataProvider itAddsElementDataProvider
     */
    public function testItAddsElement(array $array, string|int $pathKey, mixed $value, array $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($array);
        $wrapper->addElement($pathKey, $value);
        self::assertEquals($expectedResult, $wrapper->getElement($pathKey));
    }

    public function itAddsElementDataProvider(): array
    {
        return [
            [
                [],
                0,
                'Mike',
                ['Mike']
            ],
            [
                [
                    0 => [
                        'Nick'
                    ]
                ],
                0,
                'Mike',
                ['Nick', 'Mike']
            ],
        ];
    }

    public function testItDoesNotAddElement(): void
    {
        $wrapper = ArrayWrapper::create([
            'user' => [
                'name' => 'Mike'
            ]
        ]);
        $this->expectException(ElementIsNotAnArrayException::class);
        $this->expectExceptionMessage('Element [user.name] is not an array!');
        $wrapper->addElement('user.name', 'Nick');
    }

    public function testItGetsFirstElement(): void
    {
        $wrapper = ArrayWrapper::create([1, 2, 3, 4, 5]);
        self::assertEquals(1, $wrapper->getFistElement());

        $wrapper = ArrayWrapper::create([]);
        self::assertEquals('default', $wrapper->getFistElement('default'));
    }

    public function testItGetsLastElement(): void
    {
        $wrapper = ArrayWrapper::create([1, 2, 3, 4, 5]);
        self::assertEquals(5, $wrapper->getLastElement());

        $wrapper = ArrayWrapper::create([]);
        self::assertEquals('default', $wrapper->getLastElement('default'));
    }

    /**
     * @dataProvider isAssociativeDataProvider
     */
    public function testIsAssociative(array $array, bool $expectedResult): void
    {
        $wrapper = ArrayWrapper::create($array);
        self::assertEquals($expectedResult, $wrapper->isAssociative());
    }

    public function testItIterates(): void
    {
        $array = ['a', 'b', 'c'];
        $wrapper = ArrayWrapper::create($array);
        foreach ($wrapper as $key => $value) {
            self::assertEquals($value, $array[$key]);
        }
    }

    public function isAssociativeDataProvider(): array
    {
        return [
            [
                [],
                false
            ],
            [
                [1, 2, 3],
                false
            ],
            [
                [
                    0 => 1,
                    1 => 2,
                    2 => 3
                ],
                false
            ],
            [
                [
                    0 => 1,
                    2 => 3
                ],
                false
            ],
            [
                [
                    'a' => 1,
                    'b' => 2
                ],
                true
            ],
            [
                [
                    'a' => 1,
                    1 => 2
                ],
                true
            ],
            [
                [
                    '0' => 1,
                    '1' => 2
                ],
                false
            ]
        ];
    }
}
