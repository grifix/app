<?php
declare(strict_types=1);

namespace Grifix\ArrayWrapper;

use Grifix\ArrayWrapper\Exceptions\ElementIsNotAnArrayException;
use Traversable;

final class ArrayWrapper implements \IteratorAggregate
{
    private function __construct(private array $array)
    {
    }

    public static function create(array $array = []): self
    {
        return new self($array);
    }

    public function getLastElement(mixed $default = null): mixed
    {
        if (empty($this->array)) {
            return $default;
        }
        return $this->array[count($this->array)-1];
    }

    public function getFistElement(mixed $default = null): mixed
    {
        if (empty($this->array)) {
            return $default;
        }
        return $this->array[0];
    }

    public function hasElement(string|int $pathKey): bool
    {
        return $this->doHasElement(explode('.', strval($pathKey)), $this->array);
    }

    private function doHasElement(array $path, $array): bool
    {
        $firstKey = array_shift($path);
        if (false === array_key_exists($firstKey, $array)) {
            return false;
        }

        if (empty($path)) {
            return true;
        }

        if (is_array($array[$firstKey])) {
            return $this->doHasElement($path, $array[$firstKey]);
        }
        return false;
    }

    public function getElement(string|int $pathKey, $default = null): mixed
    {
        $path = explode('.', strval($pathKey));
        $result = $this->array;
        foreach ($path as $k) {
            if (is_array($result) && array_key_exists($k, $result) && $result[$k] !== null) {
                $result = $result[$k];
            } else {
                return $default;
            }
        }

        return $result;
    }

    public function addElement(string|int $pathKey, mixed $value): void
    {
        if ($this->hasElement($pathKey) && !is_array($this->getElement($pathKey))) {
            throw new ElementIsNotAnArrayException($pathKey);
        }
        if (!$this->hasElement($pathKey)) {
            $this->setElement($pathKey, [$value]);
            return;
        }
        $array = $this->getElement($pathKey);
        $array[] = $value;
        $this->setElement($pathKey, $array);
    }

    public function setElement($pathKey, $value): void
    {
        $this->doSetElement($this->array, $pathKey, $value);
    }

    public function isAssociative(): bool
    {
        foreach (array_keys($this->array) as $key) {
            if (is_string($key)) {
                return true;
            }
        }
        return false;
    }

    private static function doSetElement(array &$array, int|string $pathKey, $val): void
    {
        $path = explode('.', (string)$pathKey);
        $firstKey = array_shift($path);
        if (count($path)) {
            if (!isset($array[$firstKey]) || !is_array($array[$firstKey])) {
                $array[$firstKey] = [];
            }
            self::doSetElement($array[$firstKey], implode('.', $path), $val);
        } else {
            $array[$firstKey] = $val;
        }
    }

    public function getWrapped(): array
    {
        return $this->array;
    }

    public function getIterator(): Traversable
    {
        return new \ArrayIterator($this->array);
    }
}
