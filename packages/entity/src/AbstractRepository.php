<?php
declare(strict_types=1);

namespace Grifix\Entity;

use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractRepository
{
    public function __construct(
        private readonly EntityManagerInterface $entityManager,
        private readonly EntitySerializer $serializer
    )
    {

    }

    abstract protected function getEntityClass(): string;

    protected function doFind(string $id): ?object
    {
        $entity = $this->entityManager->find($this->getEntityClass(), $id, LockMode::PESSIMISTIC_WRITE);
        if ($entity) {
            return $this->serializer->deserialize($entity->getData());
        }
        return null;
    }

    protected function doAdd(object $aggregate): void
    {
        $entityClass = $this->getEntityClass();
        $data = $this->serializer->serialize($aggregate);
        $entity = new $entityClass($data['id']['value'], $data);
        $this->entityManager->persist($entity);
    }

    protected function doUpdate(object $aggregate): void
    {
        $data = $this->serializer->serialize($aggregate);
        /** @var AbstractEntity $entity */
        $entity = $this->entityManager->find($this->getEntityClass(), $data['id']['value']);
        $entity->setData($data);
    }
}
