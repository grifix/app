<?php
declare(strict_types=1);

namespace Grifix\Entity;

use Doctrine\ORM\Mapping as ORM;

abstract class AbstractEntity
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string")
     */
    protected string $id;

    /**
     * @ORM\Column(type="json", nullable=false, options={"jsonb"=true})
     */
    protected array $data;

    public function __construct(string $id, array $data)
    {
        $this->id = $id;
        $this->data = $data;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function getId(): string
    {
        return $this->id;
    }
}
