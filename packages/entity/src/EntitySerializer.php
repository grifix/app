<?php
declare(strict_types=1);

namespace Grifix\Entity;


use Psr\Container\ContainerInterface;

final class EntitySerializer
{
    public function __construct(private readonly ContainerInterface $container)
    {
    }

    public function serialize(object $object): array
    {
        return $this->serializeValue($object);
    }

    private function serializeObject(object $object): array
    {
        $reflection = new \ReflectionObject($object);
        $result = [
            '__type' => get_class($object)
        ];
        foreach ($reflection->getProperties() as $property) {
            if ($this->isOutside($property)) {
                continue;
            }
            $result[$property->getName()] = $this->serializeValue($this->getPropertyValue($property, $object));
        }
        return $result;
    }

    private function serializeValue($value)
    {
        if (is_object($value)) {
            return $this->serializeObject($value);
        }
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->serializeValue($v);
            }
            return $value;
        }
        return $value;
    }

    public function deserialize(array $data): object
    {
        if (false === array_key_exists('__type', $data)) {
            throw new \Exception('Type is not defined');
        }
        return $this->deserializeValue($data);
    }


    private function deserializeValue($value): mixed
    {
        if (is_array($value) && array_key_exists('__type', $value)) {
            return $this->deserializeObject($value, $value['__type']);
        }
        if (is_array($value)) {
            foreach ($value as $k => $v) {
                $value[$k] = $this->deserializeValue($v);
            }
            return $value;
        }
        return $value;
    }

    private function deserializeObject(array $data, string $type): object
    {
        $reflection = new \ReflectionClass($type);
        $result = $reflection->newInstanceWithoutConstructor();
        foreach ($reflection->getProperties() as $property) {
            if ($this->isOutside($property)) {
                $this->setPropertyValue($property, $result, $this->container->get((string)$property->getType()));
                continue;
            }
            $this->setPropertyValue($property, $result, $this->deserializeValue($data[$property->getName()] ?? null));
        }
        return $result;
    }

    private function isOutside(\ReflectionProperty $property): bool
    {
        return $property->getName() === 'outside';
    }

    private function getPropertyValue(\ReflectionProperty $property, object $object)
    {
        $property->setAccessible(true);
        $result = $property->getValue($object);
        $property->setAccessible(false);
        return $result;
    }

    private function setPropertyValue(\ReflectionProperty $property, object $object, $value): void
    {
        $property->setAccessible(true);
        $property->setValue($object, $value);
        $property->setAccessible(false);
    }
}
