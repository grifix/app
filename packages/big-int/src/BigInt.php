<?php
declare(strict_types=1);

namespace Grifix\BigInt;


use Grifix\BigInt\Exceptions\NotIntegerValueException;
use Grifix\BigInt\Exceptions\NotNumericValueException;

final class BigInt
{
    private readonly float $value;

    private function __construct(mixed $value)
    {
        if (false === is_numeric($value)) {
            throw new NotNumericValueException($value);
        }
        $value = (float)$value;
        if ($value !== floor($value)) {
            throw new NotIntegerValueException($value);
        }
        $this->value = $value;
    }

    public static function create(mixed $value): self
    {
        return new self($value);
    }

    public function isEqualTo(self $other): bool
    {
        return $this->value === $other->value;
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }

    public function toFloat(): float
    {
        return $this->value;
    }

    public function isGreaterThan(self|int|float|string $another): bool
    {
        $another = $this->convertToSelf($another);
        return $this->value > $another->value;
    }

    public function isLessThan(self|int|float|string $another): bool
    {
        $another = $this->convertToSelf($another);
        return $this->value < $another->value;
    }

    public function isLessOrEqualThan(self|int|float|string $another): bool
    {
        $another = $this->convertToSelf($another);
        return $this->value <= $another->value;
    }

    public function add(self|int|float|string $another): self
    {
        $another = $this->convertToSelf($another);
        return new self($another->value + $this->value);
    }

    public function subtract(self|int|float|string $another): self
    {
        $another = $this->convertToSelf($another);
        return new self($another->value - $this->value);
    }

    private function convertToSelf(self|int|float|string $value): self
    {
        if (!($value instanceof self)) {
            $value = self::create($value);
        }
        return $value;
    }
}
