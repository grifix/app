<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Tests;

use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\ObjectNormalizers\DateTimeNormalizer;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\DefaultObjectNormalizer;
use Grifix\Normalizer\Repository\MemoryNormalizerRepository;
use Grifix\Normalizer\Tests\Stubs\Car;
use Grifix\Normalizer\Tests\Stubs\Engine;
use Grifix\Normalizer\Tests\Stubs\Repair;
use Grifix\Normalizer\Tests\Stubs\TechnicalInspection;
use PHPUnit\Framework\TestCase;

final class NormalizerTest extends TestCase
{
    private Normalizer $normalizer;


    protected function setUp(): void
    {
        parent::setUp();
        $repository = new MemoryNormalizerRepository();
        $repository->add(new DefaultObjectNormalizer('car', Car::class, $repository));
        $repository->add(new DefaultObjectNormalizer('engine', Engine::class, $repository));
        $repository->add(new DefaultObjectNormalizer('repair', Repair::class, $repository));
        $repository->add(new DefaultObjectNormalizer('technical-inspection', TechnicalInspection::class, $repository));
        $repository->add(new DateTimeNormalizer());
        $this->normalizer = new Normalizer($repository);
    }

    /**
     * @dataProvider normalizerDataProvider
     */
    public function testItNormalizes(object $object, array $expectedResult): void
    {
        self::assertEquals($expectedResult, $this->normalizer->normalize($object));
    }

    /**
     * @dataProvider normalizerDataProvider
     */
    public function testItDenormalizes(object $expectedResult, array $data): void
    {
        self::assertEquals($expectedResult, $this->normalizer->denormalize($data));
    }

    public function normalizerDataProvider(): array
    {
        return [
            [
                new Car(
                    model: 'Audi',
                    repairs: [],
                    technicalInspections: []
                ),
                [
                    '__normalizer__' => [
                        'name' => 'car'
                    ],
                    'model' => 'Audi',
                    'engine' => null,
                    'repairs' => [],
                    'technicalInspections' => []
                ]
            ],
            [
                new Car(
                    model: 'Audi',
                    repairs: [
                        new Repair(
                            note: 'Engine'
                        ),
                        new Repair(
                            note: 'Door'
                        )
                    ],
                    technicalInspections: []
                ),
                [
                    '__normalizer__' => [
                        'name' => 'car'
                    ],
                    'model' => 'Audi',
                    'engine' => null,
                    'repairs' => [
                        [
                            '__normalizer__' => [
                                'name' => 'repair'
                            ],
                            'note' => 'Engine'
                        ],
                        [
                            '__normalizer__' => [
                                'name' => 'repair'
                            ],
                            'note' => 'Door'
                        ]
                    ],
                    'technicalInspections' => []
                ]
            ],
            [
                new Car(
                    model: 'Audi',
                    repairs: [
                        new Repair(
                            note: 'Engine'
                        ),
                        new Repair(
                            note: 'Door'
                        )
                    ],
                    technicalInspections: [
                        new TechnicalInspection(
                            date: new \DateTime('2020-01-01'),
                            notes: [
                                'one',
                                'two',
                                'three'
                            ]
                        )
                    ]
                ),
                [
                    '__normalizer__' => [
                        'name' => 'car'
                    ],
                    'model' => 'Audi',
                    'engine' => null,
                    'repairs' => [
                        [
                            '__normalizer__' => [
                                'name' => 'repair'
                            ],
                            'note' => 'Engine'
                        ],
                        [
                            '__normalizer__' => [
                                'name' => 'repair'
                            ],
                            'note' => 'Door'
                        ]
                    ],
                    'technicalInspections' => [
                        [
                            '__normalizer__' => [
                                'name' => 'technical-inspection',
                            ],
                            'notes' => [
                                'one',
                                'two',
                                'three'
                            ],
                            'date' => [
                                '__normalizer__' => [
                                    'name' => 'date-time',
                                ],
                                'value' => '2020-01-01T00:00:00+00:00'
                            ]
                        ]
                    ]
                ]
            ]
        ];
    }
}
