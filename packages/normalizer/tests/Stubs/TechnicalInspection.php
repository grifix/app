<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Stubs;

class TechnicalInspection
{
    public function __construct(public readonly \DateTime $date, public readonly array $notes)
    {
    }
}
