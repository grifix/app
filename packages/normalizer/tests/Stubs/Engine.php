<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Stubs;


class Engine
{
    public function __construct(public readonly string $type, public readonly float $volume)
    {
    }
}
