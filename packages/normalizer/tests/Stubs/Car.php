<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Tests\Stubs;


class Car
{

    /**
     * @param Repair[] $repairs
     * @param TechnicalInspection[] $technicalInspections
     */
    public function __construct(
        public readonly string $model,
        public readonly array $repairs,
        public readonly array $technicalInspections,
        public readonly ?Engine $engine = null
    )
    {
    }

}
