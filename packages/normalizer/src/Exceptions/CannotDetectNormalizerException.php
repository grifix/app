<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Exceptions;

final class CannotDetectNormalizerException extends \Exception
{

    public function __construct()
    {
        parent::__construct('Cannot detect normalizer, data array does not contain meta information about normalizer!');
    }
}
