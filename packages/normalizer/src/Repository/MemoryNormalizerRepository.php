<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Repository;

use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\Repository\Exceptions\NormalizerAlreadyExistException;
use Grifix\Normalizer\Repository\Exceptions\NormalizerDoesNotExistException;

final class MemoryNormalizerRepository implements NormalizerRepositoryInterface
{

    /** @var ObjectNormalizerInterface[] */
    private array $normalizers = [];

    public function add(ObjectNormalizerInterface $newNormalizer): void
    {
        foreach ($this->normalizers as $normalizer) {
            if ($normalizer->getName() === $newNormalizer->getName()) {
                throw NormalizerAlreadyExistException::withName($newNormalizer->getName());
            }
            if ($normalizer->getObjectClass() === $newNormalizer->getObjectClass()) {
                throw NormalizerAlreadyExistException::withObjectClass($newNormalizer->getObjectClass());
            }
        }
        $this->normalizers[] = $newNormalizer;
    }

    public function getByObjectClass(string $objectClass): ObjectNormalizerInterface
    {
        foreach($this->normalizers as $normalizer){
            if($normalizer->getObjectClass() === $objectClass){
                return $normalizer;
            }
        }
        throw NormalizerDoesNotExistException::withObjectClass($objectClass);
    }

    public function getByName(string $name): ObjectNormalizerInterface
    {
        foreach($this->normalizers as $normalizer){
            if($normalizer->getName() === $name){
                return $normalizer;
            }
        }
        throw NormalizerDoesNotExistException::withName($name);
    }

    public function hasWithName(string $name): bool
    {
        foreach($this->normalizers as $normalizer){
            if($normalizer->getName() === $name){
                return true;
            }
        }
        return false;
    }
}
