<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Repository\Exceptions;

use Exception;

final class NormalizerDoesNotExistException extends Exception
{

    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function withName(string $name): self
    {
        return new self(sprintf('Normalizer with name [%s] does not exist!', $name));
    }

    public static function withObjectClass(string $objectClass): self
    {
        return new self(sprintf('Normalizer with object class [%s] does not exist!', $objectClass));
    }
}
