<?php
declare(strict_types=1);

namespace Grifix\Normalizer\Repository;

use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;

interface NormalizerRepositoryInterface
{
    public function add(ObjectNormalizerInterface $newNormalizer):void;

    public function getByObjectClass(string $objectClass):ObjectNormalizerInterface;

    public function getByName(string $name):ObjectNormalizerInterface;

    public function hasWithName(string $name):bool;
}
