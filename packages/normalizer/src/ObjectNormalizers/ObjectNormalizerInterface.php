<?php
declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers;

interface ObjectNormalizerInterface
{
    public function normalize(object $object): array;

    public function denormalize(array $data): object;

    public function getName():string;

    public function getObjectClass():string;
}
