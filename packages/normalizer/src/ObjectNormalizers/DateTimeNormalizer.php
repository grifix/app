<?php
declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers;

use DateTimeInterface;
use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\Exceptions\InvalidObjectTypeException;

final class DateTimeNormalizer implements ObjectNormalizerInterface
{

    public function normalize(object $object): array
    {

        $result = ArrayWrapper::create();
        $result->setElement(NormalizerInterface::META_NAME_KEY, $this->getName());
        if (!($object instanceof \DateTime)) {
            throw new InvalidObjectTypeException($object::class, \DateTime::class);
        }
        $result->setElement('value', $object->format(DateTimeInterface::ATOM));
        return $result->getWrapped();
    }

    public function denormalize(array $data): object
    {
        return new \DateTime($data['value']);
    }

    public function getName(): string
    {
        return 'date-time';
    }

    public function getObjectClass(): string
    {
        return \DateTime::class;
    }
}
