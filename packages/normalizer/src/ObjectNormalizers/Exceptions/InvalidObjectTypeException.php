<?php
declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\Exceptions;

final class InvalidObjectTypeException extends \Exception
{

    public function __construct(string $givenType, string $expectedType)
    {
        parent::__construct(sprintf('Object type must be [%s] but [%s] given!', $expectedType, $givenType));
    }
}
