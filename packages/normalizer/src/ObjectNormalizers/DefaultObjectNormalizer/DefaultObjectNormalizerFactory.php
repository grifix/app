<?php
declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer;

use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;

final class DefaultObjectNormalizerFactory
{

    public function __construct(private readonly NormalizerRepositoryInterface $normalizerRepository)
    {
    }

    public function create(string $name, string $objectClass): DefaultObjectNormalizer
    {
        return new DefaultObjectNormalizer($name, $objectClass, $this->normalizerRepository);
    }
}
