<?php
declare(strict_types=1);

namespace Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Normalizer\Exceptions\CannotDetectNormalizerException;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;
use Grifix\Reflection\ReflectionObject;

final class DefaultObjectNormalizer implements ObjectNormalizerInterface
{

    public function __construct(
        private readonly string $name,
        private readonly string $objectClass,
        private readonly NormalizerRepositoryInterface $normalizerRepository
    )
    {
    }

    public function normalize(object $object): array
    {
        return $this->normalizeValue($object);
    }

    public function denormalize(array $data): object
    {
        $wrapper = ArrayWrapper::create($data);
        if ($wrapper->isAssociative() && !$wrapper->hasElement(NormalizerInterface::META_NAME_KEY)) {
            throw new CannotDetectNormalizerException();
        }
        return $this->denormalizeValue($data);
    }

    private function denormalizeValue(mixed $value): mixed
    {
        if(is_array($value)){
            $wrapper = ArrayWrapper::create($value);
            if($wrapper->hasElement(NormalizerInterface::META_NAME_KEY)){
                return $this->denormalizeObject($value);
            }
            return $this->denormalizeArray($value);
        }
        return $value;
    }

    private function denormalizeArray(array $array): array
    {
        $result = [];
        foreach ($array as $k => $v) {
            $result[$k] = $this->denormalizeValue($v);
        }
        return $result;
    }

    private function denormalizeObject(array $data): object
    {
        $wrapper = ArrayWrapper::create($data);
        $name = $wrapper->getElement(NormalizerInterface::META_NAME_KEY);
        if ($name !== $this->name) {
            return $this->normalizerRepository->getByName($name)->denormalize($data);
        }
        $reflectionClass = new \ReflectionClass($this->objectClass);
        $result = $reflectionClass->newInstanceWithoutConstructor();
        $reflectionObject = new ReflectionObject($result);
        foreach ($reflectionClass->getProperties() as $property) {
            $reflectionObject->setPropertyValue($property->getName(), $this->denormalizeValue($data[$property->getName()]));
        }
        return $result;
    }

    private function normalizeValue(mixed $value): mixed
    {
        if (is_object($value)) {
            return $this->normalizeObject($value);
        }

        if (is_array($value)) {
            return $this->normalizeArray($value);
        }

        return $value;
    }

    private function normalizeArray(array $array): array
    {
        $result = [];
        foreach ($array as $key => $value) {
            $result[$key] = $this->normalizeValue($value);
        }
        return $result;
    }

    private function normalizeObject(object $object): array
    {
        if ($object::class !== $this->objectClass) {
            return $this->normalizerRepository->getByObjectClass($object::class)->normalize($object);
        }
        $result = ArrayWrapper::create();
        $reflectionObject = new ReflectionObject($object);
        foreach ($reflectionObject->getPropertyValues() as $key => $value) {
            $result->setElement($key, $this->normalizeValue($value));
        }
        $result->setElement(NormalizerInterface::META_NAME_KEY, $this->name);
        return $result->getWrapped();
    }

    private function isAssociative(array $array): bool
    {
        return array_keys($array) !== range(0, count($array) - 1);
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getObjectClass(): string
    {
        return $this->objectClass;
    }
}
