<?php
declare(strict_types=1);

namespace Grifix\Normalizer;

use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\Normalizer\Exceptions\CannotDetectNormalizerException;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\DefaultObjectNormalizerFactory;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;

final class Normalizer implements NormalizerInterface
{

    public function __construct(
        private readonly NormalizerRepositoryInterface $normalizerRepository,
        private readonly DefaultObjectNormalizerFactory $defaultObjectNormalizerFactory
    )
    {
    }

    public function normalize(object $object): array
    {
        return $this->normalizerRepository->getByObjectClass($object::class)->normalize($object);
    }


    public function denormalize(array $data): object
    {
        $wrapper = ArrayWrapper::create($data);
        if ($wrapper->isAssociative() && !$wrapper->hasElement(self::META_NAME_KEY)) {
            throw new CannotDetectNormalizerException();
        }
        return $this->normalizerRepository->getByName($wrapper->getElement(self::META_NAME_KEY))->denormalize($data);
    }

    public function addObjectNormalizer(ObjectNormalizerInterface $normalizer): void
    {
        $this->normalizerRepository->add($normalizer);
    }

    public function addDefaultObjectNormalizer(string $name, string $objectClass): void
    {
        $this->normalizerRepository->add($this->defaultObjectNormalizerFactory->create($name, $objectClass));
    }
}
