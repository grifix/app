<?php
declare(strict_types=1);

namespace Grifix\Normalizer;

use Grifix\Normalizer\Exceptions\CannotDetectNormalizerException;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\Repository\Exceptions\NormalizerDoesNotExistException;

interface NormalizerInterface
{
    /** @internal */
    public const META_NAME_KEY = '__normalizer__.name';

    /**
     * @throws NormalizerDoesNotExistException
     */
    public function normalize(object $object): array;

    /**
     * @throws CannotDetectNormalizerException
     * @throws NormalizerDoesNotExistException
     */
    public function denormalize(array $data): object;

    public function addObjectNormalizer(ObjectNormalizerInterface $normalizer): void;

    public function addDefaultObjectNormalizer(string $name, string $objectClass): void;
}
