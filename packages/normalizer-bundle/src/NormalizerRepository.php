<?php
declare(strict_types=1);

namespace Grifix\NormalizerBundle;

use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\DefaultObjectNormalizer;
use Grifix\Normalizer\ObjectNormalizers\ObjectNormalizerInterface;
use Grifix\Normalizer\Repository\MemoryNormalizerRepository;
use Grifix\Normalizer\Repository\NormalizerRepositoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

final class NormalizerRepository implements NormalizerRepositoryInterface
{

    public function __construct(
        private readonly ContainerInterface $container,
        private readonly MemoryNormalizerRepository $normalizerRepository,
        array   $normalizers
    )
    {
        foreach ($normalizers as $normalizer) {
            if (isset($normalizer['name']) && isset($normalizer['object_class'])) {
                $this->add(new DefaultObjectNormalizer($normalizer['name'], $normalizer['object_class'], $this->normalizerRepository));
                continue;
            }
            if (isset($normalizer['class'])) {
                $this->add($this->container->get($normalizer['class']));
                continue;
            }
            throw new \Exception('Invalid normalizer configuration!');
        }
    }

    public function add(ObjectNormalizerInterface $newNormalizer): void
    {
        $this->normalizerRepository->add($newNormalizer);
    }

    public function getByObjectClass(string $objectClass): ObjectNormalizerInterface
    {
        return $this->normalizerRepository->getByObjectClass($objectClass);
    }

    public function getByName(string $name): ObjectNormalizerInterface
    {
        return $this->normalizerRepository->getByName($name);
    }

    public function hasWithName(string $name): bool
    {
        return $this->normalizerRepository->hasWithName($name);
    }
}
