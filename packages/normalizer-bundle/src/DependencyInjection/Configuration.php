<?php
declare(strict_types=1);

namespace Grifix\NormalizerBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder():TreeBuilder
    {
        $treeBuilder = new TreeBuilder('grifix_normalizer');

        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('normalizers')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('object_class')->end()
                            ->scalarNode('class')->end()
                        ->end()
                    ->end()
                ->end()
        ;

        return $treeBuilder;
    }
}
