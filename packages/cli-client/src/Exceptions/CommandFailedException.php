<?php
declare(strict_types=1);

namespace Grifix\CliClient\Exceptions;

use Exception;

final class CommandFailedException extends Exception
{
    public function __construct(string $output, int $code)
    {
        parent::__construct($output, $code);
    }
}
