<?php
declare(strict_types=1);

namespace Grifix\CliClient;


use Grifix\CliClient\Exceptions\CommandFailedException;

final class CliClient implements CliClientInterface
{
    public function __construct(private readonly string $rootDir, private readonly string $environment)
    {

    }

    public function executeCommand(string $command, ...$arguments): void
    {
        $result = 0;
        $output = '';
        exec(
            $this->createCommandString($command, ...$arguments),
            $output,
            $result
        );
        if (0 !== $result) {
            throw new CommandFailedException($output, $result);
        }
    }

    public function createCommandString(string $command, ...$arguments): string
    {
        if ($arguments) {
            $arguments = '"' . implode('" "', $arguments) . '"';
        } else {
            $arguments = '';
        }
        return sprintf(
            '%s/bin/console --no-interaction --env=%s  %s %s',
            $this->rootDir,
            $this->environment,
            $command,
            $arguments
        );
    }
}
