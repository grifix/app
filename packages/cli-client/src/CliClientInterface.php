<?php
declare(strict_types=1);

namespace Grifix\CliClient;

interface CliClientInterface
{
    public function executeCommand(string $command, ...$arguments): void;

    public function createCommandString(string $command, ...$arguments): string;
}
