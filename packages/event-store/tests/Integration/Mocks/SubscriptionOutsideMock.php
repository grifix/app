<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Mocks;

use Grifix\EventStore\Subscription\SubscriptionOutsideInterface;

final class SubscriptionOutsideMock implements SubscriptionOutsideInterface
{
    /** @var object[] */
    private array $subscribers = [];

    public function getSubscriber(string $subscriberClass): object
    {
        return $this->subscribers[$subscriberClass];
    }

    public function setSubscriber(string $subscriberClass, object $subscriber): void
    {
        $this->subscribers[$subscriberClass] = $subscriber;
    }
}
