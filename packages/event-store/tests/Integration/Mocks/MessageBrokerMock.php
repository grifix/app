<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Mocks;

use Grifix\EventStore\MessageBroker\MessageBrokerInterface;
use Throwable;

final class MessageBrokerMock implements MessageBrokerInterface
{

    /** @var callable[] */
    private array $consumers = [];

    /** @var Throwable[] */
    private array $errors = [];

    /** @var object[] */
    private array $messages = [];

    /**
     * @var ?callable;
     */
    protected $consumerSelector;

    public function send(object $message): void
    {
        $this->messages[] = $message;
    }

    public function startConsumer(callable $consumer): void
    {
        $this->consumers[] = $consumer;
    }

    public function run(): void
    {
        foreach ($this->messages as $i => $message) {
            $consumer = $this->selectConsumer($message);
            try {
                $consumer($message);
                unset($this->messages[$i]);
            } catch (Throwable $exception) {
                $this->errors[] = $exception;
            }
        }
    }

    private function selectConsumer(object $message): callable
    {
        if ($this->consumerSelector) {
            $selector = $this->consumerSelector;
            return $selector($message, $this->consumers);
        }
        $max = count($this->consumers) - 1;
        $index = mt_rand(0, $max);
        return $this->consumers[$index];
    }

    /**
     * @return Throwable[]
     */
    public function getErrors(): array
    {
        return $this->errors;
    }
}
