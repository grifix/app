<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\EventRepository;

use Grifix\EventStore\StreamType\StreamType;
use Grifix\EventStore\Tests\Integration\IntegrationTest;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events\UserDeletedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\User;
use Grifix\Uuid\Uuid;

final class EventRepositoryTest extends IntegrationTest
{
    protected function setUp(): void
    {
        parent::setUp();
        $this->eventStore->registerStreamType('user', User::class);
        $this->eventStore->registerEventType('user', 'deleted', UserDeletedEvent::class);
    }

    public function testItSplitsToChunksProperly()
    {

        for ($i = 0; $i < 100; $i++) {
            $this->eventRepository->add(
                new UserDeletedEvent('8842841a-8e88-4f5a-8c3e-968067eca09a'),
                new StreamType($this->eventTypeRepository, 'user', User::class),
                Uuid::createFromString('8842841a-8e88-4f5a-8c3e-968067eca09a')
            );
        }
        $this->eventRepository->flush();
        $this->loggerHandler->clear();
        $envelopes = iterator_to_array($this->eventRepository->find(chunkSize: 10), false);
        self::assertCount(100, $envelopes);
        self::assertCount(11, $this->loggerHandler->getRecords());
    }
}
