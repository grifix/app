<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Stubs\Repository;

abstract class Entity
{

    public function __construct(public string $id, public string $type, public string $value)
    {
    }
}
