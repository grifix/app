<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User;

use Grifix\EventStore\Tests\Integration\Stubs\Repository\Entity;

final class User extends Entity
{
    public function __construct(string $id, string $email)
    {
        parent::__construct($id, 'user', $email);
    }

    public function getEmail(): string
    {
        return $this->value;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setEmail(string $email): void
    {
        $this->value = $email;
    }
}
