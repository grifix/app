<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events;

final class UserCreatedEvent
{
    public function __construct(public readonly string $userId, public readonly string $email)
    {
    }
}
