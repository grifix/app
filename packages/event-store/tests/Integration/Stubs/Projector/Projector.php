<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Stubs\Projector;

use Doctrine\DBAL\Connection;

final class Projector
{

    public function __construct(private readonly Connection $connection)
    {
        $this->connection->executeQuery('drop table if exists projection');
        $this->connection->executeQuery('
            create table if not exists projection 
            (
                id      bigserial
                    constraint users_pk
                        primary key,
                stream_id uuid    not null,
                event_class text not null,
                payload  text
            );
        ');
    }

    public function createRecord(RecordDto $record): void
    {
        $this->connection->insert(
            'projection',
            [
                'stream_id' => $record->streamId,
                'event_class' => $record->eventClass,
                'payload' => $record->payload
            ]
        );
    }

    public function getAllRecords():array{
        $rows = $this->connection->fetchAllAssociative('select * from projection');
        if(!$rows){
            return [];
        }
        $result = [];
        foreach($rows as $row){
            $result[] = new RecordDto(
                streamId: $row['stream_id'],
                eventClass: $row['event_class'],
                payload: $row['payload'],
                id: $row['id']
            );
        }
        return $result;
    }
}
