<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Stubs\Projector;

final class RecordDto
{

    public function __construct(
        public readonly string $streamId,
        public readonly string $eventClass,
        public readonly ?string $payload = null,
        public readonly ?int $id = null
    )
    {
    }
}
