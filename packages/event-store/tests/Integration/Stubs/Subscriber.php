<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration\Stubs;

use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\Client\Events\ClientCreatedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\Client\Events\ClientDeletedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\Client\Events\ClientUpdatedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events\UserCreatedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events\UserDeletedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events\UserUpdatedEvent;

final class Subscriber
{
    private array $callbacks = [];

    public function setCallback(string $eventClass, callable $callback): void
    {
        $this->callbacks[$eventClass] = $callback;
    }

    public function removeCallback(string $eventClass): void
    {
        unset($this->callbacks[$eventClass]);
    }

    public function onUserCreated(UserCreatedEvent $event): void
    {
        $this->runCallback($event);
    }

    public function onUserUpdated(UserUpdatedEvent $event): void
    {
        $this->runCallback($event);
    }

    public function onUserDeleted(UserDeletedEvent $event): void
    {
        $this->runCallback($event);
    }

    public function onClientCreated(ClientCreatedEvent $event): void
    {
        $this->runCallback($event);
    }

    public function onClientUpdated(ClientUpdatedEvent $event): void
    {
        $this->runCallback($event);
    }

    public function onClientDeleted(ClientDeletedEvent $event): void
    {
        $this->runCallback($event);
    }

    private function runCallback(object $event): void
    {
        if (isset($this->callbacks[$event::class])) {
            $callback = $this->callbacks[$event::class];
            $callback($event);
        }
    }
}
