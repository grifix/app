<?php
/** @noinspection SqlResolve */
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration;

use Doctrine\DBAL\Exception\DriverException;
use Grifix\ArrayWrapper\ArrayWrapper;
use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\Client\Client;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\Client\Events\ClientCreatedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\Client\Events\ClientDeletedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\Client\Events\ClientUpdatedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events\UserCreatedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events\UserDeletedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\Events\UserUpdatedEvent;
use Grifix\EventStore\Tests\Integration\Stubs\EventProducers\User\User;
use Grifix\EventStore\Tests\Integration\Stubs\Projector\Projector;
use Grifix\EventStore\Tests\Integration\Stubs\Projector\RecordDto;
use Grifix\EventStore\Tests\Integration\Stubs\Repository\Repository;
use Grifix\EventStore\Tests\Integration\Stubs\Subscriber;
use Grifix\Uuid\Uuid;

final class EventStoreTest extends IntegrationTest
{
    private Repository $repository;

    private Projector $projector;

    private const SUBSCRIPTION_TYPE = 'test_subscription';

    protected function setUp(): void
    {
        parent::setUp();

        $this->repository = new Repository($this->connection);
        $this->projector = new Projector($this->connection);
        $this->registerStreamsAndEvents();
        $this->registerSubscription();

        $this->messageBroker->startConsumer(function (EventEnvelope $envelope) {
            $this->eventStore->processEvent($envelope);
        });

    }

    private function registerStreamsAndEvents(): void
    {
        $this->eventStore->registerStreamType('user', User::class);
        $this->eventStore->registerEventType('user', 'created', UserCreatedEvent::class);
        $this->eventStore->registerEventType('user', 'updated', UserUpdatedEvent::class);
        $this->eventStore->registerEventType('user', 'deleted', UserDeletedEvent::class);

        $this->eventStore->registerStreamType('client', Client::class);
        $this->eventStore->registerEventType('client', 'created', ClientCreatedEvent::class);
        $this->eventStore->registerEventType('client', 'updated', ClientUpdatedEvent::class);
        $this->eventStore->registerEventType('client', 'deleted', ClientDeletedEvent::class);
    }

    public function registerSubscription(): void
    {
        $this->eventStore->registerSubscriptionType(
            self::SUBSCRIPTION_TYPE,
            Subscriber::class,
            'user',
            [
                'user.created',
            ],
            [
                'user.deleted',
            ]
        );
    }

    private function setUpSagaSubscriber(): void
    {
        $this->subscriber->setCallback(UserCreatedEvent::class, function (UserCreatedEvent $event) {
            $this->connection->beginTransaction();
            $client = new Client($event->userId, $event->email);
            $this->repository->add($client);
            $this->eventStore->storeEvent(
                new ClientCreatedEvent($client->getId(), $client->getName()),
                Client::class,
                Uuid::createFromString($client->getId())
            );
            $this->eventStore->flush();
            $this->connection->commit();
        });
        $this->subscriber->setCallback(UserUpdatedEvent::class, function (UserUpdatedEvent $event) {
            $this->connection->beginTransaction();
            $client = $this->repository->findClient($event->userId);
            $client->setName($event->newEmail);
            $this->eventStore->storeEvent(
                new ClientUpdatedEvent($client->getId(), $client->getName()),
                Client::class,
                Uuid::createFromString($client->getId())
            );
            $this->repository->update($client);
            $this->eventStore->flush();
            $this->connection->commit();
        });
        $this->subscriber->setCallback(UserDeletedEvent::class, function (UserDeletedEvent $event) {
            $this->connection->beginTransaction();
            $client = $this->repository->findClient($event->userId);
            $this->repository->delete($client->getId());
            $this->eventStore->storeEvent(
                new ClientDeletedEvent($client->getId()),
                Client::class,
                Uuid::createFromString($client->getId())
            );
            $this->eventStore->flush();
            $this->connection->commit();
        });
    }

    private function setUpProjectorSubscriber(): void
    {
        $this->subscriber->setCallback(
            UserCreatedEvent::class,
            function (UserCreatedEvent $event) {
                $this->projector->createRecord(new RecordDto($event->userId, $event::class, $event->email));
            }
        );

        $this->subscriber->setCallback(
            UserUpdatedEvent::class,
            function (UserUpdatedEvent $event) {
                $this->projector->createRecord(new RecordDto($event->userId, $event::class, $event->newEmail));
            }
        );

        $this->subscriber->setCallback(
            UserDeletedEvent::class,
            function (UserDeletedEvent $event) {
                $this->projector->createRecord(new RecordDto($event->userId, $event::class));
            }
        );
    }

    public function testItStoresEvents(): void
    {
        $this->clock->freezeTime(new \DateTimeImmutable('2020-01-01 00:00:00.000000'));
        $event = new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com');
        $this->eventStore->storeEvent(
            $event,
            User::class,
            Uuid::createFromString('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
        $this->eventStore->flush();

        $envelopes = iterator_to_array($this->eventRepository->find());
        /** @var EventEnvelope $envelope */
        $envelope = $envelopes[0];
        self::assertCount(1, $envelopes);
        self::assertEquals(Uuid::createFromString('8e9e733e-c3df-48d8-b1c1-86388967b335'), $envelope->streamId);
        self::assertEquals($event, $envelope->event);
        self::assertEquals(BigInt::create(1), $envelope->number);
        self::assertEquals('user', $envelope->streamTypeName);
        self::assertEquals('created', $envelope->eventTypeName);
        self::assertEquals(new \DateTimeImmutable('2020-01-01 00:00:00.000000'), $envelope->dateOfCreation);
        self::assertEquals(null, $envelope->dateOfPublishing);
    }

    public function testItCreatesSubscription(): void
    {
        $this->setUpSagaSubscriber();
        $this->publishEvent(
            new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );

        $this->loggerHandler->clear();
        $this->messageBroker->run();

        self::assertEquals('Beginning transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO entities (id, type, value) VALUES (?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select max(number) as number from grifix_event_store.events where stream_id=? and stream_type_name=?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.events (id, stream_type_name, stream_id, number, date_of_creation, event_type_name, event) VALUES (?, ?, ?, ?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.subscriptions (type, stream_id, last_received_event_number, status) VALUES (?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Committing transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);

        /** @var EventEnvelope[] $events */
        $events = iterator_to_array($this->eventRepository->find());
        self::assertCount(2, $events);
        self::assertEquals('user.created', $events[0]->getEventType());
        self::assertEquals('client.created', $events[1]->getEventType());

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, '8e9e733e-c3df-48d8-b1c1-86388967b335');
        self::assertTrue($subscription->isActive());
        self::assertEquals(1, $subscription->getLastReceivedEventNumber()->toFloat());
        self::assertEquals(
            new Client('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            $this->repository->findClient('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
    }

    public function testItUpdatesSubscription(): void
    {
        $this->setUpSagaSubscriber();
        $this->publishEvent(
            new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        $this->publishEvent(
            new UserUpdatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'new@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->loggerHandler->clear();
        $this->messageBroker->run();

        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Connecting with parameters {params}', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Beginning transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from entities where id=? and type=? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select max(number) as number from grifix_event_store.events where stream_id=? and stream_type_name=?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'UPDATE entities SET type = ?, value = ? WHERE id = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.events (id, stream_type_name, stream_id, number, date_of_creation, event_type_name, event) VALUES (?, ?, ?, ?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'UPDATE grifix_event_store.subscriptions SET stream_id = ?, last_received_event_number = ?, status = ? WHERE stream_id = ? AND type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Committing transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);

        /** @var EventEnvelope[] $events */
        $events = iterator_to_array($this->eventRepository->find());
        self::assertCount(4, $events);
        self::assertEquals('client.updated', $events[3]->getEventType());

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, '8e9e733e-c3df-48d8-b1c1-86388967b335');
        self::assertTrue($subscription->isActive());
        self::assertEquals(2, $subscription->getLastReceivedEventNumber()->toFloat());
        self::assertEquals(
            new Client('8e9e733e-c3df-48d8-b1c1-86388967b335', 'new@test.com'),
            $this->repository->findClient('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
    }

    public function testItFinishesSubscription(): void
    {
        $this->setUpSagaSubscriber();
        $this->publishEvent(
            new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        $this->publishEvent(
            new UserUpdatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'new@test.com'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        $this->publishEvent(
            new UserDeletedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335'),
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );

        $this->loggerHandler->clear();
        $this->messageBroker->run();

        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Connecting with parameters {params}', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Beginning transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from entities where id=? and type=? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'DELETE FROM entities WHERE id = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select max(number) as number from grifix_event_store.events where stream_id=? and stream_type_name=?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'INSERT INTO grifix_event_store.events (id, stream_type_name, stream_id, number, date_of_creation, event_type_name, event) VALUES (?, ?, ?, ?, ?, ?, ?)',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'select * from grifix_event_store.subscriptions where stream_id = ? and type = ? for update',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals(
            'UPDATE grifix_event_store.subscriptions SET stream_id = ?, last_received_event_number = ?, status = ? WHERE stream_id = ? AND type = ?',
            $this->loggerHandler->shiftRecord()['context']['sql']
        );
        self::assertEquals('Committing transaction', $this->loggerHandler->shiftRecord()['message']);
        self::assertEquals('Disconnecting', $this->loggerHandler->shiftRecord()['message']);

        /** @var EventEnvelope[] $events */
        $events = iterator_to_array($this->eventRepository->find());
        self::assertCount(6, $events);
        self::assertEquals('client.deleted', $events[5]->getEventType());

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, '8e9e733e-c3df-48d8-b1c1-86388967b335');
        self::assertTrue($subscription->isFinished());
        self::assertEquals(3, $subscription->getLastReceivedEventNumber()->toFloat());
        self::assertEquals(
            null,
            $this->repository->findClient('8e9e733e-c3df-48d8-b1c1-86388967b335')
        );
    }

    public function testItDoesNotCreateSubscriptionOnSqlError(): void
    {
        $this->subscriber->setCallback(UserCreatedEvent::class, function (UserCreatedEvent $event) {
            $this->connection->update('not_existing_table', ['name' => 'joe'], ['id' => 1]);
        });

        $event = new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com');
        $this->publishEvent(
            $event,
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        /** @var EventEnvelope[] $events */
        $events = iterator_to_array($this->eventRepository->find());
        self::assertCount(1, $events);
        self::assertEquals($event, $events[0]->event);
        self::assertCount(1, $this->messageBroker->getErrors());
        self::assertInstanceOf(DriverException::class, $this->messageBroker->getErrors()[0]);
        self::assertEquals(0, $this->subscriptionRepository->count());
    }

    public function testItCreatesSubscriptionOnDomainError(): void
    {
        $this->subscriber->setCallback(UserCreatedEvent::class, function (UserCreatedEvent $event) {
            throw new \Exception('Domain exception!');
        });

        $event = new UserCreatedEvent('8e9e733e-c3df-48d8-b1c1-86388967b335', 'user@test.com');
        $this->publishEvent(
            $event,
            User::class,
            '8e9e733e-c3df-48d8-b1c1-86388967b335'
        );
        $this->messageBroker->run();

        /** @var EventEnvelope[] $events */
        $events = iterator_to_array($this->eventRepository->find());
        self::assertCount(1, $events);
        self::assertEquals($event, $events[0]->event);
        self::assertCount(1, $this->messageBroker->getErrors());
        self::assertEquals('Domain exception!', $this->messageBroker->getErrors()[0]->getMessage());
        self::assertEquals(1, $this->subscriptionRepository->count());
        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, '8e9e733e-c3df-48d8-b1c1-86388967b335');
        self::assertTrue($subscription->isActive());
        self::assertEquals(1, $subscription->getLastReceivedEventNumber()->toFloat());
    }

    public function testItProcessesMissedEvents(): void
    {
        $this->setUpProjectorSubscriber();
        $events = [
            new UserCreatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test@grifix.net'),
            new UserUpdatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test1@grifix.net'),
            new UserUpdatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test2@grifix.net'),
            new UserUpdatedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e', 'test3@grifix.net'),
            new UserDeletedEvent('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e')
        ];
        foreach ($events as $event) {
            $this->eventStore->storeEvent(
                $event,
                User::class,
                Uuid::createFromString('05cff34b-6bde-4bdd-9c5e-eb41fcc0441e')
            );
        }

        $this->eventStore->flush();

        $envelopes = ArrayWrapper::create(iterator_to_array($this->eventRepository->find(), false));
        $this->messageBroker->send($envelopes->getFistElement());
        $this->messageBroker->send($envelopes->getLastElement());

        $this->loggerHandler->clear();
        $this->messageBroker->run();

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e');
        self::assertTrue($subscription->isFinished());
        $records = $this->projector->getAllRecords();
        self::assertCount(5, $records);
        self::assertEquals(
            [
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserCreatedEvent::class,
                    'test@grifix.net',
                    1
                ),
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserUpdatedEvent::class,
                    'test1@grifix.net',
                    2
                ),
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserUpdatedEvent::class,
                    'test2@grifix.net',
                    3
                ),
                new RecordDto(
                    '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    UserUpdatedEvent::class,
                    'test3@grifix.net',
                    4
                ),
                new RecordDto(
                    streamId: '05cff34b-6bde-4bdd-9c5e-eb41fcc0441e',
                    eventClass: UserDeletedEvent::class,
                    id: 5
                )
            ],
            $records
        );
    }

    public function testFinishedSubscriptionDoesNotProcessEvents(): void
    {
        $this->setUpProjectorSubscriber();
        $this->publishEvent(
            new UserCreatedEvent(
                'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                'old_email'
            ),
            User::class,
            'b41ae221-8fcf-4fac-8811-86c2778b59e1'
        );
        $this->publishEvent(
            new UserDeletedEvent(
                'b41ae221-8fcf-4fac-8811-86c2778b59e1',
            ),
            User::class,
            'b41ae221-8fcf-4fac-8811-86c2778b59e1'
        );

        $this->publishEvent(
            new UserUpdatedEvent(
                'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                'new_email'
            ),
            User::class,
            'b41ae221-8fcf-4fac-8811-86c2778b59e1'
        );

        $this->messageBroker->run();

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, 'b41ae221-8fcf-4fac-8811-86c2778b59e1');
        self::assertEquals(2, $subscription->getLastReceivedEventNumber()->toFloat());
        $rows = $this->projector->getAllRecords();
        self::assertCount(2, $rows);
        self::assertEquals(
            [
                new RecordDto(
                    'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                    UserCreatedEvent::class,
                    'old_email',
                    1
                ),
                new RecordDto(
                    'b41ae221-8fcf-4fac-8811-86c2778b59e1',
                    UserDeletedEvent::class,
                    id: 2
                ),
            ],
            $rows
        );
    }

    public function testItPausesSubscription(): void
    {
        $this->publishEvent(
            new UserCreatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();
        $this->eventStore->pauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        self::assertTrue($subscription->isPaused());
    }

    public function testItUnpausesSubscription(): void
    {
        $this->publishEvent(
            new UserCreatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();
        $this->eventStore->pauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();

        $this->eventStore->unpauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        self::assertTrue($subscription->isActive());
    }

    public function testPausedSubscriptionDoesNotProcessEvents(): void
    {
        $this->publishEvent(
            new UserCreatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();

        $this->eventStore->pauseSubscription(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        $this->eventStore->flush();

        $this->publishEvent(
            new UserUpdatedEvent(
                'b74e494a-e927-44dd-8831-efca79c0b877',
                'test2'
            ),
            User::class,
            'b74e494a-e927-44dd-8831-efca79c0b877'
        );
        $this->messageBroker->run();

        $subscription = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, 'b74e494a-e927-44dd-8831-efca79c0b877');
        self::assertEquals(1, $subscription->getLastReceivedEventNumber()->toFloat());
    }

    public function testSubscriptionDoesNotProcessEventsFromTheWrongStream(): void
    {
        $streamA = '43ea45a3-0b51-4450-8332-790720abbed8';
        $streamB = 'ac4305ab-3a27-4523-a493-f6f1a7d126ed';
        $this->publishEvent(
            new UserCreatedEvent(
                $streamA,
                'email'
            ),
            User::class,
            $streamA
        );

        $this->publishEvent(
            new UserUpdatedEvent(
                $streamA,
                'new_email'
            ),
            User::class,
            $streamA
        );

        $this->publishEvent(
            new UserCreatedEvent(
                $streamB,
                'email2'
            ),
            User::class,
            $streamB
        );

        $this->messageBroker->run();

        $subscriptionA = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, $streamA);
        $subscriptionB = $this->subscriptionRepository->get(self::SUBSCRIPTION_TYPE, $streamB);
        self::assertEquals(2, $subscriptionA->getLastReceivedEventNumber()->toFloat());
        self::assertEquals(1, $subscriptionB->getLastReceivedEventNumber()->toFloat());
    }

    public function testSubscriptionDoesNotCreateOnNonStartingEvent(): void
    {
        $this->publishEvent(
            new UserUpdatedEvent(
                'd6ef55d3-b89c-4120-8cf8-e3ba52e17753',
                'new_email'
            ),
            User::class,
            'd6ef55d3-b89c-4120-8cf8-e3ba52e17753'
        );
        $this->messageBroker->run();
        self::assertEquals(0, $this->subscriptionRepository->count());
    }

    private function publishEvent(object $event, string $producerClass, string $producerId): void
    {
        $this->eventStore->storeEvent(
            $event,
            $producerClass,
            Uuid::createFromString($producerId)
        );
        $this->eventStore->flush();
        $this->eventStore->publishEvents();
    }
}
