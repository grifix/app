<?php
declare(strict_types=1);

namespace Grifix\EventStore\Tests\Integration;

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DriverManager;
use Doctrine\DBAL\Logging\Middleware;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Grifix\Clock\FrozenClock;
use Grifix\EventStore\Event\Repository\DbalEventRepository;
use Grifix\EventStore\EventStore;
use Grifix\EventStore\EventType\Repository\MemoryEventTypeRepository;
use Grifix\EventStore\Migrations\Version20220423084315;
use Grifix\EventStore\StreamType\Repository\MemoryStreamTypeRepository;
use Grifix\EventStore\Subscription\Repository\SubscriptionRepository;
use Grifix\EventStore\Subscription\SubscriptionFactory;
use Grifix\EventStore\Subscription\SubscriptionNormalizer;
use Grifix\EventStore\SubscriptionType\Repository\MemorySubscriptionTypeRepository;
use Grifix\EventStore\Tests\Integration\Mocks\LoggerHandler;
use Grifix\EventStore\Tests\Integration\Mocks\MessageBrokerMock;
use Grifix\EventStore\Tests\Integration\Mocks\SubscriptionOutsideMock;
use Grifix\EventStore\Tests\Integration\Stubs\Subscriber;
use Grifix\EventStore\Workers\EventProcessor;
use Grifix\EventStore\Workers\EventPublisher;
use Grifix\Normalizer\Normalizer;
use Grifix\Normalizer\NormalizerInterface;
use Grifix\Normalizer\ObjectNormalizers\DateTimeImmutableNormalizer;
use Grifix\Normalizer\ObjectNormalizers\DefaultObjectNormalizer\DefaultObjectNormalizerFactory;
use Grifix\Normalizer\Repository\MemoryNormalizerRepository;
use Monolog\Logger;
use PHPUnit\Framework\TestCase;

abstract class IntegrationTest extends TestCase
{
    private array $migrations = [
        Version20220423084315::class
    ];

    protected readonly Connection $connection;

    protected readonly Connection $connection2;

    protected readonly Logger $logger;

    protected readonly EventStore $eventStore;

    protected readonly MemoryStreamTypeRepository $streamTypeRepository;

    protected readonly MemoryEventTypeRepository $eventTypeRepository;

    protected readonly MemorySubscriptionTypeRepository $subscriptionTypeRepository;

    protected readonly DbalEventRepository $eventRepository;

    protected readonly FrozenClock $clock;

    protected readonly NormalizerInterface $normalizer;

    protected readonly SubscriptionRepository $subscriptionRepository;

    protected readonly SubscriptionOutsideMock $subscriptionOutside;

    protected readonly SubscriptionNormalizer $subscriptionNormalizer;

    protected readonly EventPublisher $eventPublisher;

    protected readonly MessageBrokerMock $messageBroker;

    protected readonly EventProcessor $eventProcessor;

    protected readonly SubscriptionFactory $subscriptionFactory;

    protected readonly MemoryNormalizerRepository $normalizerRepository;

    protected readonly Subscriber $subscriber;

    protected readonly LoggerHandler $loggerHandler;

    protected function setUp(): void
    {
        parent::setUp();
        $this->loggerHandler = new LoggerHandler();
        $this->logger = new Logger('logger', [$this->loggerHandler]);
        $this->connection = $this->createConnection();
        $this->connection2 = $this->createConnection();
        $this->subscriber = new Subscriber();
        $this->subscriptionOutside = new SubscriptionOutsideMock();
        $this->subscriptionOutside->setSubscriber(Subscriber::class, $this->subscriber);
        $this->streamTypeRepository = new MemoryStreamTypeRepository();
        $this->subscriptionTypeRepository = new MemorySubscriptionTypeRepository();
        $this->normalizerRepository = new MemoryNormalizerRepository();
        $this->normalizer = new Normalizer($this->normalizerRepository, new DefaultObjectNormalizerFactory($this->normalizerRepository));
        $this->normalizer->addObjectNormalizer(new DateTimeImmutableNormalizer());
        $this->eventTypeRepository = new MemoryEventTypeRepository($this->normalizer);
        $this->clock = new FrozenClock(new \DateTimeImmutable('2020-01-01 00:00:00.000001'));

        $this->eventRepository = new DbalEventRepository($this->connection, $this->clock, $this->eventTypeRepository, $this->normalizer);
        $this->subscriptionNormalizer = new SubscriptionNormalizer($this->subscriptionOutside, $this->subscriptionTypeRepository);
        $this->subscriptionRepository = new SubscriptionRepository($this->connection, $this->subscriptionNormalizer);
        $this->messageBroker = new MessageBrokerMock();
        $this->subscriptionFactory = new SubscriptionFactory(
            $this->subscriptionOutside,
            $this->subscriptionTypeRepository
        );
        $this->eventPublisher = new EventPublisher($this->eventRepository, $this->messageBroker);
        $this->eventProcessor = new EventProcessor(
            $this->connection,
            $this->subscriptionRepository,
            $this->subscriptionTypeRepository,
            $this->subscriptionFactory,
            $this->eventRepository,
            $this->logger
        );
        $this->eventStore = new EventStore(
            $this->streamTypeRepository,
            $this->eventTypeRepository,
            $this->subscriptionTypeRepository,
            $this->eventRepository,
            $this->subscriptionRepository,
            $this->eventPublisher,
            $this->eventProcessor
        );
        $this->runMigrations();
    }

    private function createConnection(): Connection
    {
        $config = new Configuration();
        $config->setMiddlewares([
            new Middleware($this->logger)
        ]);
        return DriverManager::getConnection(
            [
                'dbname' => $_ENV['PHPUNIT_DB_NAME'],
                'user' => $_ENV['PHPUNIT_DB_USER'],
                'password' => $_ENV['PHPUNIT_DB_PASSWORD'],
                'host' => $_ENV['PHPUNIT_DB_HOST'],
                'driver' => 'pdo_pgsql'
            ],
            $config
        );
    }

    private function runMigrations(): void
    {
        if ($this->schemaExists('grifix_event_store')) {
            foreach ($this->migrations as $migrationClass) {
                /** @var AbstractMigration $migration */
                $migration = new $migrationClass($this->connection, $this->logger);
                $migration->down(new Schema());
                $this->executeMigration($migration);
            }
        }

        foreach ($this->migrations as $migrationClass) {
            /** @var AbstractMigration $migration */
            $migration = new $migrationClass($this->connection, $this->logger);
            $migration->up(new Schema());
            $this->executeMigration($migration);
        }
    }

    private function executeMigration(AbstractMigration $migration): void
    {
        foreach ($migration->getSql() as $sql) {
            $this->connection->executeQuery($sql->getStatement(), $sql->getParameters(), $sql->getTypes());
        }
    }

    private function schemaExists(string $schema): bool
    {
        return $this->connection->fetchOne(
            'SELECT EXISTS(SELECT 1 FROM information_schema.schemata WHERE schema_name = :schema)',
            ['schema' => $schema]
        );
    }
}
