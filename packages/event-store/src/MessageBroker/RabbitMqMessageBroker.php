<?php
declare(strict_types=1);

namespace Grifix\EventStore\MessageBroker;

use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Workers\EventProcessor;
use Grifix\Normalizer\NormalizerInterface;
use PhpAmqpLib\Channel\AMQPChannel;
use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;

final class RabbitMqMessageBroker implements MessageBrokerInterface
{
    private readonly AMQPChannel $channel;

    public function __construct(
        private readonly AMQPStreamConnection $connection,
        private readonly NormalizerInterface $normalizer
    )
    {
        $this->channel = $this->connection->channel();
    }

    public function send(object $message): void
    {
        $this->createQueue();
        $msg = new AMQPMessage(
            json_encode($this->normalizer->normalize($message)),
            [
                'delivery_mode' => AMQPMessage::DELIVERY_MODE_PERSISTENT
            ]
        );
        $this->channel->basic_publish($msg, '', self::QUEUE);
    }

    public function startConsumer(callable $consumer): void
    {
        $this->createQueue();
        $this->channel->basic_consume(
            queue: self::QUEUE,

            callback: function (AMQPMessage $msg) use ($consumer) {
                /** @var EventEnvelope $envelope */
                $envelope = $this->normalizer->denormalize(json_decode($msg->body, true));
                $consumer($envelope);
                $msg->ack();
            }
        );
        while ($this->channel->is_open()) {
            $this->channel->wait();
        }
        $this->close();
    }

    private function createQueue(): void
    {
        $this->channel->queue_declare(
            queue: self::QUEUE,
            durable: true,
        );
    }

    private function close(): void
    {
        $this->channel->close();
        $this->connection->close();
    }

    public function __destruct()
    {
        $this->close();
    }
}
