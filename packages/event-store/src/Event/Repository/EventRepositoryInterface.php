<?php
declare(strict_types=1);

namespace Grifix\EventStore\Event\Repository;


use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\StreamType\StreamType;
use Grifix\Uuid\Uuid;
use phpDocumentor\Reflection\Types\Boolean;

interface EventRepositoryInterface
{
    /**
     * @return EventEnvelope[]
     */
    public function find(
        ?bool   $published = null,
        ?BigInt $fromNumber = null,
        string  $streamId = null,
        ?int $limit = null,
        int    $chunkSize = 1000
    ): iterable;

    public function add(object $event, StreamType $streamType, Uuid $streamId): void;

    public function markAsSent(string $eventId): void;

    public function flush(): void;
}
