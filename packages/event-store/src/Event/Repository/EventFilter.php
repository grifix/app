<?php
declare(strict_types=1);

namespace Grifix\EventStore\Event\Repository;

final class EventFilter
{
    public function __construct(
        public readonly ?bool $published = null
    )
    {
    }
}
