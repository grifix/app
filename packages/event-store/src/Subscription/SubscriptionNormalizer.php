<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription;


use Grifix\BigInt\BigInt;
use Grifix\EventStore\Subscription\Status\SubscriptionStatus;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;
use Grifix\Reflection\ReflectionObject;
use Grifix\StateMachine\StateMachine;
use Grifix\Uuid\Uuid;
use JetBrains\PhpStorm\ArrayShape;
use ReflectionClass;

final class SubscriptionNormalizer
{
    public function __construct(
        private readonly SubscriptionOutsideInterface $outside,
        private readonly SubscriptionTypeRepositoryInterface $subscriptionTypeRepository
    )
    {
    }

    #[ArrayShape(
        [
            'type' => "string",
            'stream_id' => "string",
            'last_received_event_number' => "string",
            'status' => "string"
        ]
    )]
    public function normalize(Subscription $subscription): array
    {
        $reflection = new ReflectionObject($subscription);
        return [
            'type' => $reflection->getPropertyValue('type.name'),
            'stream_id' => $reflection->getPropertyValue('streamId.value'),
            'last_received_event_number' => $reflection->getPropertyValue('lastReceivedEventNumber.value'),
            'status' => $reflection->getPropertyValue('status.value.state')
        ];
    }

    public function denormalize(array $data): Subscription
    {
        /** @var Subscription $result */
        $result = (new ReflectionClass(Subscription::class))->newInstanceWithoutConstructor();
        $reflection = new ReflectionObject($result);
        $reflection->setPropertyValue('streamId', Uuid::createFromString($data['stream_id']));
        $reflection->setPropertyValue('type', $this->subscriptionTypeRepository->getByName($data['type']));
        $reflection->setPropertyValue('lastReceivedEventNumber', BigInt::create($data['last_received_event_number']));
        $reflection->setPropertyValue('outside', $this->outside);
        $reflection->setPropertyValue('status', $this->denormalizeStatus($data['status']));
        return $result;
    }

    public function denormalizeStatus(string $status): SubscriptionStatus
    {
        /** @var SubscriptionStatus $result */
        $result = (new ReflectionClass(SubscriptionStatus::class))->newInstanceWithoutConstructor();
        $reflection = new ReflectionObject($result);
        $reflection->setPropertyValue('value', $this->denormalizeStateMachine($status));
        return $result;

    }

    public function denormalizeStateMachine(string $state): StateMachine
    {
        /** @var StateMachine $result */
        $result = (new ReflectionClass(StateMachine::class))->newInstanceWithoutConstructor();
        $reflection = new ReflectionObject($result);
        $reflection->setPropertyValue('state', $state);
        $reflection->setPropertyValue('possibleStates', SubscriptionStatus::getPossibleStates());
        $reflection->setPropertyValue('transitions', SubscriptionStatus::getTransitions());
        return $result;
    }
}
