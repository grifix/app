<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription;

use Grifix\BigInt\BigInt;
use Grifix\CallableListener\CallableListener;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Subscription\Exceptions\CannotFinishSubscriptionException;
use Grifix\EventStore\Subscription\Exceptions\CannotPauseSubscriptionException;
use Grifix\EventStore\Subscription\Exceptions\EventIsNotStartingException;
use Grifix\EventStore\Subscription\Exceptions\EventTypeIsNotSupportedException;
use Grifix\EventStore\Subscription\Exceptions\InvalidEventNumberException;
use Grifix\EventStore\Subscription\Exceptions\SubscriptionIsNotActiveException;
use Grifix\EventStore\Subscription\Exceptions\WrongEventStreamException;
use Grifix\EventStore\Subscription\Status\Exceptions\CannotChangeSubscriptionStatus;
use Grifix\EventStore\Subscription\Status\SubscriptionStatus;
use Grifix\EventStore\SubscriptionType\SubscriptionType;
use Grifix\Uuid\Uuid;

final class Subscription
{
    private readonly Uuid $streamId;

    private SubscriptionStatus $status;

    private BigInt $lastReceivedEventNumber;

    public function __construct(
        private       readonly SubscriptionOutsideInterface $outside,
        private       readonly SubscriptionType $type,
        EventEnvelope $startingEventEnvelope,
    )
    {
        $this->streamId = $startingEventEnvelope->streamId;
        $this->status = SubscriptionStatus::create($this->streamId);
        $this->lastReceivedEventNumber = $startingEventEnvelope->number->subtract(1);
        if (!$this->type->isStartingEventType($startingEventEnvelope->getEventType())) {
            throw new EventIsNotStartingException($startingEventEnvelope->getEventType(), (string)$this->type);
        }
    }

    /**
     * @throws CannotFinishSubscriptionException
     * @throws EventTypeIsNotSupportedException
     * @throws InvalidEventNumberException
     * @throws SubscriptionIsNotActiveException
     * @throws WrongEventStreamException
     */
    public function processEvent(EventEnvelope $envelope): void
    {
        $this->assertIsActive();
        $this->assertEventTypeIsSupported($envelope->getEventType());
        $this->assertEventIsFromProperStream($envelope);
        if ($envelope->number->isLessOrEqualThan($this->lastReceivedEventNumber)) {
            return;
        }
        $this->assertEventHasProperNumber($envelope->number);
        $this->lastReceivedEventNumber = $envelope->number;
        if ($this->type->isFinishingEventType($envelope->getEventType())) {
            $this->finish();
        }
        $subscriber = $this->outside->getSubscriber($this->type->subscriberClass);
        $listener = new CallableListener($subscriber);
        if ($listener->isEventSupported($envelope->event::class)) {
            $listener($envelope->event);
        }
    }

    private function finish(): void
    {
        try {
            $this->status = $this->status->finish();
        } catch (CannotChangeSubscriptionStatus $exception) {
            throw new CannotFinishSubscriptionException(
                (string)$this->type,
                (string)$this->streamId,
                $exception->fromStatus
            );
        }

    }

    public function pause(): void
    {
        try {
            $this->status = $this->status->pause();
        } catch (CannotChangeSubscriptionStatus $exception) {
            throw new CannotPauseSubscriptionException(
                (string)$this->type,
                (string)$this->streamId,
                $exception->fromStatus
            );
        }
    }

    public function unpause(): void
    {
        try {
            $this->status = $this->status->unpause();
        } catch (CannotChangeSubscriptionStatus $exception) {
            throw new CannotPauseSubscriptionException(
                (string)$this->type,
                (string)$this->streamId,
                $exception->fromStatus
            );
        }
    }

    public function isActive(): bool
    {
        return $this->status->isActive();
    }

    public function isFinished(): bool
    {
        return $this->status->isFinished();
    }

    public function isPaused():bool{
        return $this->status->isPaused();
    }

    private function assertEventHasProperNumber(BigInt $number): void
    {
        $expectedNumber = $this->calculateNextExpectedEventNumber();
        if (!$number->isEqualTo($expectedNumber)) {
            throw new InvalidEventNumberException((string)$this->streamId, (string)$number, (string)$expectedNumber);
        }
    }

    public function calculateNextExpectedEventNumber(): BigInt
    {
        return $this->lastReceivedEventNumber->add(1);
    }

    private function assertEventTypeIsSupported(string $eventType): void
    {
        if (!$this->type->isEventTypeSupported($eventType)) {
            throw new EventTypeIsNotSupportedException($eventType, (string)$this->type, (string)$this->streamId);
        }
    }

    private function assertEventIsFromProperStream(EventEnvelope $envelope): void
    {
        if (!$envelope->streamId->isEqualTo($this->streamId)) {
            throw new WrongEventStreamException(
                (string)$this->type,
                (string)$this->streamId,
                (string)$envelope->streamId,
                $envelope->getEventType()
            );
        }
    }

    private function assertIsActive(): void
    {
        if (!$this->status->isActive()) {
            throw new SubscriptionIsNotActiveException((string)$this->streamId);
        }
    }

    public function getStreamId(): string
    {
        return (string)$this->streamId;
    }

    public function getType(): string
    {
        return (string)$this->type;
    }

    public function getLastReceivedEventNumber(): BigInt
    {
        return $this->lastReceivedEventNumber;
    }
}
