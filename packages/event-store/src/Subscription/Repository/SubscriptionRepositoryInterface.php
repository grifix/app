<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Repository;

use Grifix\EventStore\Subscription\Repository\Exceptions\SubscriptionDoesNotExitsException;
use Grifix\EventStore\Subscription\Subscription;

interface SubscriptionRepositoryInterface
{
    public function flush(): void;

    /**o
     * @throws SubscriptionDoesNotExitsException
     */
    public function get(string $subscriptionType, string $streamId): Subscription;

    public function add(Subscription $subscription): void;

    public function count(): int;

    public function delete(string $subscriptionType, string $streamId): void;

}
