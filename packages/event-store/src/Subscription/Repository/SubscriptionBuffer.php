<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Repository;

use Grifix\EventStore\Subscription\Subscription;

final class SubscriptionBuffer implements \IteratorAggregate
{
    private array $subscriptions = [];

    public function add(Subscription $subscription): void
    {
        $this->subscriptions[$this->createKey($subscription->getType(), $subscription->getStreamId())] = $subscription;
    }

    public function get($subscriptionType, $streamId): ?Subscription
    {
        $key = $this->createKey($subscriptionType, $streamId);
        if (isset($this->subscriptions[$key])) {
            return $this->subscriptions[$key];
        }
        return null;
    }

    public function remove($subscriptionType, $streamId): void
    {
        unset($this->subscriptions[$this->createKey($subscriptionType, $streamId)]);
    }

    private function createKey($subscriptionType, $subscriptionId): string
    {
        return $subscriptionType . '.' . $subscriptionId;
    }

    public function getIterator(): \Traversable
    {
        return new \ArrayIterator($this->subscriptions);
    }

    public function isEmpty(): bool
    {
        return empty($this->subscriptions);
    }

}
