<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Repository;

use Doctrine\DBAL\Connection;
use Grifix\EventStore\Subscription\Repository\Exceptions\SubscriptionDoesNotExitsException;
use Grifix\EventStore\Subscription\Subscription;
use Grifix\EventStore\Subscription\SubscriptionNormalizer;

final class SubscriptionRepository implements SubscriptionRepositoryInterface
{
    private const TABLE = 'grifix_event_store.subscriptions';

    private SubscriptionBuffer $buffer;

    public function __construct(
        private readonly Connection $connection,
        private readonly SubscriptionNormalizer $normalizer
    )
    {
        $this->buffer = new SubscriptionBuffer();
    }


    public function flush(): void
    {
        while (!$this->buffer->isEmpty()) {
            foreach ($this->buffer as $subscription) {
                /** @var Subscription $subscription */
                $this->persistSubscription($subscription);
                $this->buffer->remove($subscription->getType(), $subscription->getStreamId());
            }
        }
    }

    /**
     * @throws SubscriptionDoesNotExitsException
     */
    public function get(string $subscriptionType, string $streamId): Subscription
    {
        $result = $this->buffer->get($subscriptionType, $streamId);
        if (null !== $result) {
            return $result;
        }

        $data = $this->getData($subscriptionType, $streamId);
        if (null === $data) {
            throw new SubscriptionDoesNotExitsException($subscriptionType, $streamId);
        }
        $result = $this->normalizer->denormalize($data);
        $this->buffer->add($result);
        return $result;
    }

    public function count(): int
    {
        return $this->connection->fetchOne('select count(*) from ' . self::TABLE);
    }

    private function getData(string $subscriptionType, string $streamId): ?array
    {
        $result = $this->connection->fetchAssociative(
            'select * from ' . self::TABLE . ' where stream_id = :stream_id and type = :type for update',
            [
                'stream_id' => $streamId,
                'type' => $subscriptionType
            ]
        );
        if (!$result) {
            return null;
        }
        return $result;
    }

    public function add(Subscription $subscription): void
    {
        $this->buffer->add($subscription);
    }

    public function delete(string $subscriptionType, string $streamId): void
    {
        $this->connection->delete(
            self::TABLE,
            [
                'type' => $subscriptionType,
                'stream_id' => $streamId
            ]
        );
    }

    private function persistSubscription(Subscription $subscription): void
    {
        $data = $this->normalizer->normalize($subscription);
        if (null === $this->getData($subscription->getType(), $subscription->getStreamId())) {
            $this->connection->insert(
                self::TABLE,
                $data
            );
            return;
        }
        unset($data['type'], $data['streamId']);

        $this->connection->update(
            self::TABLE,
            $data,
            [
                'stream_id' => $subscription->getStreamId(),
                'type' => $subscription->getType()
            ]
        );
    }
}
