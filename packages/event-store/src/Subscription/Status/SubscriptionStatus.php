<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Status;

use Grifix\EventStore\Subscription\Status\Exceptions\CannotChangeSubscriptionStatus;
use Grifix\StateMachine\Exceptions\TransitionIsImpossibleException;
use Grifix\StateMachine\StateMachine;
use Grifix\StateMachine\Transition;
use Grifix\Uuid\Uuid;

final class SubscriptionStatus
{
    private const ACTIVE = 'active';

    private const PAUSED = 'paused';

    private const FINISHED = 'finished';


    private function __construct(private readonly StateMachine $value)
    {

    }

    public static function create(Uuid $subscriptionId): self
    {
        return new self(
            new StateMachine(
                self::ACTIVE,
                self::getPossibleStates(),
                self::getTransitions()
            )
        );
    }

    /**
     * @internal
     */
    public static function getTransitions(): array
    {
        return [
            new Transition(null, self::ACTIVE),
            new Transition(self::ACTIVE, self::PAUSED),
            new Transition(self::ACTIVE, self::FINISHED),
            new Transition(self::PAUSED, self::ACTIVE)
        ];
    }

    /**
     * @internal
     */
    public static function getPossibleStates(): array
    {
        return [
            self::ACTIVE,
            self::PAUSED,
            self::FINISHED
        ];
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    public function pause(): self
    {
        return $this->changeValue(self::PAUSED);
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    public function unpause(): self
    {
        return $this->changeValue(self::ACTIVE);
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    public function finish(): self
    {
        return $this->changeValue(self::FINISHED);
    }

    public function isActive(): bool
    {
        return (string)$this->value === self::ACTIVE;
    }

    public function isFinished(): bool
    {
        return (string)$this->value === self::FINISHED;
    }

    public function isPaused(): bool
    {
        return (string)$this->value === self::PAUSED;
    }

    /**
     * @throws CannotChangeSubscriptionStatus
     */
    private function changeValue(string $newState): self
    {
        $newValue = clone $this->value;
        try {
            $newValue->makeTransition($newState);
        } catch (TransitionIsImpossibleException $exception) {
            throw new CannotChangeSubscriptionStatus($exception->fromState, $exception->toState);
        }

        return new self($newValue);
    }

    public function __toString(): string
    {
        return (string)$this->value;
    }
}
