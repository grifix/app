<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription;

interface SubscriptionOutsideInterface
{
    public function getSubscriber(string $subscriberClass):object;
}
