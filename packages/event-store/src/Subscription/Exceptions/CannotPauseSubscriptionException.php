<?php
declare(strict_types=1);

namespace Grifix\EventStore\Subscription\Exceptions;

use Exception;

final class CannotPauseSubscriptionException extends Exception
{

    public function __construct(string $subscriptionType, string $streamId, string $currentStatus)
    {
        parent::__construct(
            sprintf(
                'Cannot pause subscription [%s] for stream [%s] because it\'s status is [%s]',
                $subscriptionType,
                $streamId,
                $currentStatus
            ),
        );
    }
}
