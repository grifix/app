<?php
declare(strict_types=1);

namespace Grifix\EventStore;

use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Event\Repository\EventRepositoryInterface;
use Grifix\EventStore\EventType\EventType;
use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;
use Grifix\EventStore\StreamType\Repository\StreamTypeRepositoryInterface;
use Grifix\EventStore\StreamType\StreamType;
use Grifix\EventStore\Subscription\Repository\SubscriptionRepositoryInterface;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;
use Grifix\EventStore\SubscriptionType\SubscriptionType;
use Grifix\EventStore\Workers\EventProcessor;
use Grifix\EventStore\Workers\EventPublisher;
use Grifix\Uuid\Uuid;

final class EventStore implements EventStoreInterface
{
    public function __construct(
        private readonly StreamTypeRepositoryInterface $streamTypeRepository,
        private readonly EventTypeRepositoryInterface $eventTypeRepository,
        private readonly SubscriptionTypeRepositoryInterface $subscriptionTypeRepository,
        private readonly EventRepositoryInterface $eventRepository,
        private readonly SubscriptionRepositoryInterface $subscriptionRepository,
        private readonly EventPublisher $eventPublisher,
        private readonly EventProcessor $eventProcessor
    )
    {
    }

    public function registerStreamType(string $name, string $producerClass): void
    {
        $this->streamTypeRepository->add(
            new StreamType($this->eventTypeRepository, $name, $producerClass)
        );
    }

    public function registerEventType(string $streamTypeName, string $name, string $eventClass): void
    {
        $this->eventTypeRepository->add(new EventType($streamTypeName, $name, $eventClass));
    }

    public function registerSubscriptionType(
        string $name,
        string $subscriberClass,
        string $streamType,
        array  $staringEvents,
        array  $finishingEvents = []
    ): void
    {
        $this->subscriptionTypeRepository->add(new SubscriptionType(
            $name,
            $subscriberClass,
            $this->streamTypeRepository->getByName($streamType),
            $staringEvents,
            $finishingEvents
        ));
    }

    public function unpauseSubscription(?string $subscriptionType, ?string $subscriptionId): void
    {
        $this->subscriptionRepository->get($subscriptionType, $subscriptionId)->unpause();
    }

    public function pauseSubscription(string $subscriptionType, string $subscriptionId): void
    {
        $this->subscriptionRepository->get($subscriptionType, $subscriptionId)->pause();
    }

    public function resetSubscription(?string $subscriptionType, ?string $subscriptionId): void
    {
        // TODO: Implement resetSubscription() method.
    }

    public function storeEvent(object $event, string $producerClass, Uuid $streamId): void
    {
        $streamType = $this->streamTypeRepository->getByProducerClass($producerClass);
        $this->eventRepository->add($event, $streamType, $streamId);
    }

    public function flush(): void
    {
        $this->eventRepository->flush();
        $this->subscriptionRepository->flush();
    }

    public function publishEvents(): void
    {
        $this->eventPublisher->__invoke();
    }

    public function processEvent(EventEnvelope $envelope): void
    {
        $this->eventProcessor->__invoke($envelope);
    }
}
