<?php
declare(strict_types=1);

namespace Grifix\EventStore\SubscriptionType\Repository\Exceptions;

final class SubscriptionTypeAlreadyExistsException extends \Exception
{

    public function __construct(string $name)
    {
        parent::__construct(sprintf('Subscription type with name [%s[ already exists!', $name));
    }
}
