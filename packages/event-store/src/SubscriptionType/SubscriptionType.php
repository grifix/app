<?php
declare(strict_types=1);

namespace Grifix\EventStore\SubscriptionType;

use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;
use Grifix\EventStore\StreamType\StreamType;
use Grifix\Uuid\Uuid;

final class SubscriptionType
{
    /**
     * @param string[] $startingEventTypes
     * @param string[] $finishingEventTypes
     */
    public function __construct(
        public readonly string $name,
        public readonly string $subscriberClass,
        public readonly StreamType $streamType,
        public readonly array $startingEventTypes = [],
        public readonly array $finishingEventTypes = [],
    )
    {
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function isFinishingEventType(string $eventType): bool
    {
        return in_array($eventType, $this->finishingEventTypes);
    }

    public function isStartingEventType(string $eventType): bool
    {
        return in_array($eventType, $this->startingEventTypes);
    }

    public function isEventTypeSupported(string $eventType): bool{
        return $this->streamType->isEventTypeSupported($eventType);
    }
}
