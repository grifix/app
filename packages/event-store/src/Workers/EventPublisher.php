<?php
declare(strict_types=1);

namespace Grifix\EventStore\Workers;

use Grifix\EventStore\Event\Repository\EventRepositoryInterface;
use Grifix\EventStore\MessageBroker\MessageBrokerInterface;

final class EventPublisher
{

    public function __construct(
        private readonly EventRepositoryInterface $eventRepository,
        private readonly MessageBrokerInterface $messageBroker,
    )
    {
    }

    public function __invoke(): void
    {
        foreach ($this->eventRepository->find(published: false, chunkSize: 10000) as $eventEnvelope) {
            $this->messageBroker->send($eventEnvelope);
            $this->eventRepository->markAsSent((string)$eventEnvelope->id);
        }
    }
}
