<?php
declare(strict_types=1);

namespace Grifix\EventStore\Workers;

use Doctrine\DBAL\Connection;
use Grifix\BigInt\BigInt;
use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\Event\Repository\EventRepositoryInterface;
use Grifix\EventStore\Subscription\Exceptions\InvalidEventNumberException;
use Grifix\EventStore\Subscription\Repository\Exceptions\SubscriptionDoesNotExitsException;
use Grifix\EventStore\Subscription\Repository\SubscriptionRepositoryInterface;
use Grifix\EventStore\Subscription\Subscription;
use Grifix\EventStore\Subscription\SubscriptionFactory;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;
use Grifix\EventStore\SubscriptionType\SubscriptionType;
use Psr\Log\LoggerInterface;
use Throwable;

final class EventProcessor
{

    public function __construct(
        private readonly Connection $connection,
        private readonly SubscriptionRepositoryInterface $subscriptionRepository,
        private readonly SubscriptionTypeRepositoryInterface $subscriptionTypeRepository,
        private readonly SubscriptionFactory $subscriptionFactory,
        private readonly EventRepositoryInterface $eventRepository,
        private readonly LoggerInterface $logger
    )
    {
    }

    public function __invoke(EventEnvelope $envelope): void
    {
        $this->connection->connect();
        foreach ($this->subscriptionTypeRepository->findByStreamType($envelope->streamTypeName) as $subscriptionType) {
            $this->processSubscription($envelope, $subscriptionType);
        }
        $this->connection->close();
    }

    private function processSubscription($envelope, $subscriptionType): void
    {
        $this->connection->beginTransaction();
        try {
            $this->processEvent($envelope, $subscriptionType);
        }
        finally {
            try {
                $this->subscriptionRepository->flush();
                $this->connection->commit();
            } catch (Throwable $exception) {
                $this->connection->rollBack();
                throw $exception;
            }
        }
    }

    private function processEvent(EventEnvelope $envelope, SubscriptionType $subscriptionType): void
    {
        $subscription = $this->getSubscription($subscriptionType, $envelope);
        if (!$subscription) {
            return;
        }
        try {
            $subscription->processEvent($envelope);
        } catch (InvalidEventNumberException $exception) {
            $this->logger->notice($exception->getMessage());
            $this->processMissedEvents(BigInt::create($exception->expectedNumber), $subscription);
        }
    }

    private function processMissedEvents(BigInt $fromNumber, Subscription $subscription): void
    {
        foreach ($this->eventRepository->find(
            fromNumber: $fromNumber,
            streamId: $subscription->getStreamId(),
            limit: 1000
        ) as $envelope) {
            $subscription->processEvent($envelope);
        }
    }

    private function getSubscription(SubscriptionType $subscriptionType, EventEnvelope $envelope): ?Subscription
    {
        try {
            return $this->subscriptionRepository->get((string)$subscriptionType, (string)$envelope->streamId);
        } catch (SubscriptionDoesNotExitsException) {
            if ($subscriptionType->isStartingEventType($envelope->getEventType())) {
                return $this->createNewSubscription($subscriptionType, $envelope);
            }
            return null;
        }
    }

    private function createNewSubscription(SubscriptionType $subscriptionType, EventEnvelope $envelope): Subscription
    {
        $result = $this->subscriptionFactory->create((string)$subscriptionType, $envelope);
        $this->subscriptionRepository->add($result);
        return $result;
    }
}
