<?php
declare(strict_types=1);

namespace Grifix\EventStore;

use Grifix\EventStore\Event\EventEnvelope;
use Grifix\Uuid\Uuid;

interface EventStoreInterface
{
    public function registerStreamType(string $name, string $producerClass): void;

    public function registerEventType(string $streamTypeName, string $name, string $eventClass): void;

    public function registerSubscriptionType(
        string $name,
        string $subscriberClass,
        string $streamType,
        array  $staringEvents,
        array  $finishingEvents = []
    ): void;

    public function unpauseSubscription(string $subscriptionType, string $subscriptionId): void;

    public function pauseSubscription(string $subscriptionType, string $subscriptionId): void;

    public function resetSubscription(string $subscriptionType, string $subscriptionId): void;

    public function storeEvent(object $event, string $producerClass, Uuid $streamId): void;

    public function flush(): void;

    public function publishEvents(): void;

    public function processEvent(EventEnvelope $envelope): void;
}

