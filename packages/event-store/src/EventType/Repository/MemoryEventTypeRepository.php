<?php
declare(strict_types=1);

namespace Grifix\EventStore\EventType\Repository;

use Grifix\EventStore\EventType\EventType;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeAlreadyExistsException;
use Grifix\EventStore\EventType\Repository\Exceptions\EventTypeDoesNotExistException;
use Grifix\Normalizer\NormalizerInterface;

final class MemoryEventTypeRepository implements EventTypeRepositoryInterface
{


    /**
     * @var EventType[]
     */
    private array $eventTypes = [];

    public function __construct(private readonly NormalizerInterface $normalizer)
    {
    }

    public function add(EventType $newEventType): void
    {
        foreach ($this->eventTypes as $eventType) {
            if ((string)$eventType === (string) $newEventType) {
                throw EventTypeAlreadyExistsException::withName((string) $newEventType);
            }
            if ($eventType->eventClass === $newEventType->eventClass) {
                throw EventTypeAlreadyExistsException::withEventClass($newEventType->eventClass);
            }
        }
        $this->eventTypes[] = $newEventType;
        $this->normalizer->addDefaultObjectNormalizer((string) $newEventType, $newEventType->eventClass);
    }

    public function getByEventClass(string $eventClass): EventType
    {
        foreach ($this->eventTypes as $eventType) {
            if ($eventType->eventClass === $eventClass) {
                return $eventType;
            }
        }
        throw EventTypeDoesNotExistException::forEventClass($eventClass);
    }

    public function getByEventType(string $eventType): EventType
    {
        foreach ($this->eventTypes as $existingEventType) {
           if((string) $existingEventType === $eventType){
               return $existingEventType;
           }
        }
        throw EventTypeDoesNotExistException::forEventType($eventType);
    }
}
