<?php
declare(strict_types=1);

namespace Grifix\EventStore\EventType\Repository\Exceptions;

use Exception;

final class EventTypeAlreadyExistsException extends Exception
{

    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function withName(string $name): self
    {
        return new self(sprintf('Event type with name "%s" already exists!', $name));
    }

    public static function withEventClass(string $eventClass): self
    {
        return new self(sprintf('Event type with event class "%s" already exists!', $eventClass));
    }
}
