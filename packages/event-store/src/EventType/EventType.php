<?php
declare(strict_types=1);

namespace Grifix\EventStore\EventType;

final class EventType
{
    public function __construct(
        public readonly string $streamTypeName,
        public readonly string $name,
        public readonly string $eventClass
    )
    {
    }

    public function __toString(): string
    {
        return $this->streamTypeName . '.' . $this->name;
    }
}
