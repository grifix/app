<?php
declare(strict_types=1);

namespace Grifix\EventStore\Cli;

use Grifix\EventStore\EventStoreInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class PauseSubscriptionCommand extends Command
{
    public const NAME = 'grifix:event-store:pause-subscription';
    public const ARG_SUBSCRIPTION_TYPE = 'subscriptionType';
    public const ARG_STREAM_ID = 'streamId';
    protected static $defaultName = self::NAME;
    protected static $defaultDescription = 'pause subscription';

    public function __construct(private readonly EventStoreInterface $eventStore)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(self::ARG_SUBSCRIPTION_TYPE, InputArgument::REQUIRED)
            ->addArgument(self::ARG_STREAM_ID, InputArgument::REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->eventStore->pauseSubscription($input->getArgument(self::ARG_STREAM_ID), $input->getArgument(self::ARG_SUBSCRIPTION_TYPE));
        return self::SUCCESS;
    }
}
