<?php
declare(strict_types=1);

namespace Grifix\EventStore\Cli;

use Grifix\EventStore\EventStoreInterface;
use Grifix\Worker\AbstractWorkerCommand;
use Grifix\Worker\WorkerFactory;
use Grifix\Worker\WorkerFactoryInterface;

final class RunEventPublisherWorkerCommand extends AbstractWorkerCommand
{
    public const NAME = 'grifix:event-store:run-event-publisher-worker';
    protected static $defaultName = self::NAME;
    protected static $defaultDescription = 'Sends all unpublished events to the message broker';

    public function __construct(
        private       readonly EventStoreInterface $eventStore,
        WorkerFactoryInterface $workerFactory
    )
    {
        parent::__construct($workerFactory);
    }

    protected function getCallback(): callable
    {
        return function () {
            $this->eventStore->publishEvents();
        };
    }
}
