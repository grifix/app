<?php
declare(strict_types=1);

namespace Grifix\EventStore\Cli;

use Grifix\EventStore\Event\EventEnvelope;
use Grifix\EventStore\EventStoreInterface;
use Grifix\EventStore\MessageBroker\MessageBrokerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

final class RunEventConsumerCommand extends Command
{
    public const NAME = 'grifix:event-store:run-event-consumer';
    protected static $defaultName = self::NAME;
    protected static $defaultDescription = 'Consumes published messages';

    public function __construct(
        private readonly MessageBrokerInterface $messageBroker,
        private readonly EventStoreInterface $eventStore,
    )
    {
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->messageBroker->startConsumer(function (EventEnvelope $envelope) {
            $this->eventStore->processEvent($envelope);
        });
        return self::SUCCESS;
    }
}
