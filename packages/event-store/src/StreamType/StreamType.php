<?php
declare(strict_types=1);

namespace Grifix\EventStore\StreamType;

use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;

final class StreamType
{

    public function __construct(
        private readonly EventTypeRepositoryInterface $eventTypeRepository,
        public  readonly string $name,
        public  readonly string $producerClass
    )
    {
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function hasSameName(string $name): bool
    {
        return $this->name === $name;
    }

    public function hasSameProducerClass(string $producerClass): bool
    {
        return $this->producerClass === $producerClass;
    }

    public function isEventTypeSupported(string $eventType): bool
    {
        $eventType = $this->eventTypeRepository->getByEventType($eventType);
        return $eventType->streamTypeName === $this->name;
    }
}
