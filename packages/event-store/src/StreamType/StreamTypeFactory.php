<?php
declare(strict_types=1);

namespace Grifix\EventStore\StreamType;

use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;

final class StreamTypeFactory
{

    public function __construct(private readonly EventTypeRepositoryInterface $eventTypeRepository)
    {
    }

    public function create(string $name, string $producerClass): StreamType
    {
        return new StreamType($this->eventTypeRepository, $name, $producerClass);
    }
}
