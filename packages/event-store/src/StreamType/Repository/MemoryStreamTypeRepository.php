<?php
declare(strict_types=1);

namespace Grifix\EventStore\StreamType\Repository;

use Grifix\EventStore\StreamType\Repository\Exceptions\StreamTypeAlreadyExistsException;
use Grifix\EventStore\StreamType\Repository\Exceptions\StreamTypeDoesNotExist;
use Grifix\EventStore\StreamType\StreamType;

final class MemoryStreamTypeRepository implements StreamTypeRepositoryInterface
{

    /**
     * @var StreamType[]
     */
    private array $streamTypes = [];


    public function add(StreamType $newStreamType): void
    {
        foreach ($this->streamTypes as $streamType) {
            if ($streamType->hasSameName($newStreamType->name)) {
                throw StreamTypeAlreadyExistsException::withName($newStreamType->name);
            }
            if ($streamType->hasSameProducerClass($newStreamType->producerClass)) {
                throw StreamTypeAlreadyExistsException::withProducerClass($newStreamType->producerClass);
            }
        }
        $this->streamTypes[] = $newStreamType;
    }

    public function getByProducerClass(string $producerClass): StreamType
    {
        foreach ($this->streamTypes as $streamType) {
            if ($streamType->producerClass === $producerClass) {
                return $streamType;
            }
        }
        throw StreamTypeDoesNotExist::withProducerClass($producerClass);
    }

    public function getByName(string $name):StreamType{
        foreach ($this->streamTypes as $streamType) {
            if ($streamType->name === $name) {
                return $streamType;
            }
        }
        throw StreamTypeDoesNotExist::withName($name);
    }
}
