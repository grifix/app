<?php
declare(strict_types=1);

namespace Grifix\EventStore\StreamType\Repository\Exceptions;

final class StreamTypeDoesNotExist extends \Exception
{
    private function __construct(string $message)
    {
        parent::__construct($message);
    }

    public static function withProducerClass(string $producerClass): self
    {
        return new self(sprintf('Stream type with producer class [%s] does not exist', $producerClass));
    }

    public static function withName(string $name): self
    {
        return new self(sprintf('Stream type with name [%s] does not exist', $name));
    }
}
