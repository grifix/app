<?php
declare(strict_types=1);

namespace Grifix\ProcessManger\Exceptions;

final class CannotStopProcessException extends \Exception
{
    public function __construct($message)
    {
        parent::__construct($message);
    }
}
