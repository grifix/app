<?php
declare(strict_types=1);

namespace Grifix\ProcessManger\Exceptions;

final class ProcessDoesNotExistException extends \Exception
{

    public function __construct(public readonly string $processId)
    {
        parent::__construct(sprintf('Process [%s] does not exist!', $processId));
    }
}
