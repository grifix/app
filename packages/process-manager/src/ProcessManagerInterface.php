<?php
declare(strict_types=1);

namespace Grifix\ProcessManger;

use Grifix\ProcessManger\Exceptions\CannotStartProcessException;
use Grifix\ProcessManger\Exceptions\CannotStopProcessException;
use Grifix\ProcessManger\Exceptions\ProcessAlreadyExistsException;
use Grifix\ProcessManger\Exceptions\ProcessDoesNotExistException;

interface ProcessManagerInterface
{
    public function processExists(string $processId): bool;

    /**
     * @throws CannotStartProcessException
     * @throws ProcessAlreadyExistsException
     */
    public function startProcess(string $processId, string $command): void;

    /**
     * @throws CannotStopProcessException
     * @throws ProcessDoesNotExistException
     */
    public function stopProcess(string $processId): void;

    public function findProcesses(?string $filter = null): array;
}
