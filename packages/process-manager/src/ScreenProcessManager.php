<?php
declare(strict_types=1);

namespace Grifix\ProcessManger;

use Grifix\ProcessManger\Exceptions\CannotStartProcessException;
use Grifix\ProcessManger\Exceptions\CannotStopProcessException;
use Grifix\ProcessManger\Exceptions\ProcessAlreadyExistsException;
use Grifix\ProcessManger\Exceptions\ProcessDoesNotExistException;
use Grifix\ProcessManger\Exceptions\TooLongProcessIdException;

final class ScreenProcessManager implements ProcessManagerInterface
{

    private const MAX_PROCESS_ID_LENGTH = 80;

    public function processExists(string $processId): bool
    {
        return boolval(shell_exec(sprintf('screen -list | grep "%s"', $processId)));
    }

    /**
     * @throws CannotStartProcessException
     * @throws ProcessAlreadyExistsException
     * @throws TooLongProcessIdException
     */
    public function startProcess(string $processId, string $command): void
    {
        $this->assertProcessIdLength($processId);
        $result = null;
        $output = [];
        if ($this->processExists($processId)) {
            throw new ProcessAlreadyExistsException($processId);
        }
        exec(sprintf('screen -dm -S "%s" %s', $processId, $command), $output, $result);

        if ($result !== 0) {
            throw new CannotStartProcessException(implode(' ', $output));
        }
    }


    /**
     * @throws CannotStopProcessException
     * @throws ProcessDoesNotExistException
     */
    public function stopProcess(string $processId): void
    {
        if (false === $this->processExists($processId)) {
            throw new ProcessDoesNotExistException($processId);
        }
        $result = null;
        $output = [];
        exec(sprintf('screen -X -S "%s" quit', $processId), $output, $result);
        if ($result !== 0) {
            throw new CannotStopProcessException(implode(' ', $output));
        }
    }

    public function findProcesses(?string $filter = null): array
    {
        $result = [];
        $command = 'screen -list';
        if ($filter) {
            $command .= sprintf(' | grep "%s"', $filter);
        }
        $output = shell_exec($command);
        if (empty($output)) {
            return [];
        }
        $output = explode("\n", $output);
        if (null === $filter) {
            array_shift($output);
        }
        foreach ($output as $row) {
            if (empty($row)) {
                continue;
            }
            $row = explode('.', $row);
            $row = explode("\t", $row[1]);
            $result[] = $row[0];
        }
        return $result;
    }

    private function assertProcessIdLength($processId): void
    {
        if (strlen($processId) > self::MAX_PROCESS_ID_LENGTH) {
            throw new TooLongProcessIdException(self::MAX_PROCESS_ID_LENGTH, $processId);
        }
    }
}
