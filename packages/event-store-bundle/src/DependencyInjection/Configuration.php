<?php
declare(strict_types=1);

namespace Grifix\EventStoreBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

final class Configuration implements ConfigurationInterface
{

    public function getConfigTreeBuilder(): TreeBuilder
    {
        $treeBuilder = new TreeBuilder('grifix_event_store');

        // @formatter:off
        $treeBuilder->getRootNode()
            ->children()
                ->arrayNode('message_broker')
                    ->children()
                        ->scalarNode('host')->end()
                        ->integerNode('port')->end()
                        ->scalarNode('user')->end()
                        ->scalarNode('password')->end()
                    ->end()
                ->end()
                ->arrayNode('streams')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('producer_class')->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('events')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('stream')->end()
                            ->scalarNode('event_class')->end()
                        ->end()
                    ->end()
                ->end()

                ->arrayNode('subscriptions')
                    ->arrayPrototype()
                        ->children()
                            ->scalarNode('name')->end()
                            ->scalarNode('stream')->end()
                            ->scalarNode('subscriber_class')->end()
                            ->arrayNode('starting_events')->scalarPrototype()->end()->end()
                            ->arrayNode('stopping_events')->scalarPrototype()->end()->end()
                        ->end()
                    ->end()
                ->end()
        ;
        // @formatter:on

        return $treeBuilder;
    }
}
