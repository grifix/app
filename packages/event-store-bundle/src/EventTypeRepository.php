<?php
declare(strict_types=1);

namespace Grifix\EventStoreBundle;

use Grifix\EventStore\EventType\EventType;
use Grifix\EventStore\EventType\Repository\EventTypeRepositoryInterface;
use Grifix\EventStore\EventType\Repository\MemoryEventTypeRepository;

final class EventTypeRepository implements EventTypeRepositoryInterface
{

    public function __construct(
        private readonly MemoryEventTypeRepository $eventTypeRepository,
        array   $eventTypes
    )
    {
        foreach ($eventTypes as $eventType) {
            $this->add(
                new EventType(
                    $eventType['stream'],
                    $eventType['name'],
                    $eventType['event_class']
                )
            );
        }
    }

    public function add(EventType $newEventType): void
    {
        $this->eventTypeRepository->add($newEventType);
    }

    public function getByEventClass(string $eventClass): EventType
    {
        return $this->eventTypeRepository->getByEventClass($eventClass);
    }

    public function getByEventType(string $eventType): EventType
    {
        return $this->eventTypeRepository->getByEventType($eventType);
    }
}
