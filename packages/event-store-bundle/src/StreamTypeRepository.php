<?php
declare(strict_types=1);

namespace Grifix\EventStoreBundle;

use Grifix\EventStore\StreamType\Repository\MemoryStreamTypeRepository;
use Grifix\EventStore\StreamType\Repository\StreamTypeRepositoryInterface;
use Grifix\EventStore\StreamType\StreamType;
use Grifix\EventStore\StreamType\StreamTypeFactory;

final class StreamTypeRepository implements StreamTypeRepositoryInterface
{
    public function __construct(
        private readonly MemoryStreamTypeRepository $streamTypeRepository,
        private readonly StreamTypeFactory $streamTypeFactory,
        array   $streamTypes
    )
    {
        foreach ($streamTypes as $streamType) {
            $this->add($this->streamTypeFactory->create($streamType['name'], $streamType['producer_class']));
        }
    }

    public function add(StreamType $newStreamType): void
    {
        $this->streamTypeRepository->add($newStreamType);
    }

    public function getByProducerClass(string $producerClass): StreamType
    {
        return $this->streamTypeRepository->getByProducerClass($producerClass);
    }

    public function getByName(string $name): StreamType
    {
        return $this->streamTypeRepository->getByName($name);
    }
}
