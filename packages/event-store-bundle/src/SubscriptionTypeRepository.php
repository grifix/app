<?php
declare(strict_types=1);

namespace Grifix\EventStoreBundle;

use Grifix\EventStore\StreamType\Repository\StreamTypeRepositoryInterface;
use Grifix\EventStore\SubscriptionType\Repository\MemorySubscriptionTypeRepository;
use Grifix\EventStore\SubscriptionType\Repository\SubscriptionTypeRepositoryInterface;
use Grifix\EventStore\SubscriptionType\SubscriptionType;

final class SubscriptionTypeRepository implements SubscriptionTypeRepositoryInterface
{

    public function __construct(
        private readonly MemorySubscriptionTypeRepository $subscriptionTypeRepository,
        private readonly StreamTypeRepositoryInterface $streamTypeRepository,
        array   $subscriptionTypes)
    {
        foreach ($subscriptionTypes as $subscriptionType) {
            $this->subscriptionTypeRepository->add(
                new SubscriptionType(
                    $subscriptionType['name'],
                    $subscriptionType['subscriber_class'],
                    $this->streamTypeRepository->getByName($subscriptionType['stream']),
                    $subscriptionType['starting_events'],
                    $subscriptionType['stopping_events']
                )
            );
        }
    }

    public function add(SubscriptionType $newSubscriptionType): void
    {
        $this->subscriptionTypeRepository->add($newSubscriptionType);
    }

    public function findByStreamType(string $streamType): array
    {
        return $this->subscriptionTypeRepository->findByStreamType($streamType);
    }

    public function getByName(string $name): SubscriptionType
    {
        return $this->subscriptionTypeRepository->getByName($name);
    }
}
