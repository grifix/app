<?php
declare(strict_types=1);

namespace Grifix\EventStoreBundle;

use Grifix\EventStore\MessageBroker\MessageBrokerInterface;
use Grifix\EventStore\Subscription\SubscriptionOutsideInterface;
use Psr\Container\ContainerInterface;

final class SubscriptionOutside implements SubscriptionOutsideInterface
{
    public function __construct(
        private readonly ContainerInterface $container,
    )
    {
    }

    public function getSubscriber(string $subscriberClass): object
    {
        return $this->container->get($subscriberClass);
    }
}
