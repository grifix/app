<?php
declare(strict_types=1);

namespace Grifix\ProcessManagerBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

final class GrifixProcessManagerBundle extends Bundle
{
}

