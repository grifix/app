<?php
declare(strict_types=1);

namespace Grifix\StateMachine\Exceptions;

final class TransitionIsImpossibleException extends \Exception
{

    public function __construct(public readonly ?string $fromState, public readonly ?string $toState)
    {
        parent::__construct(sprintf('Transition from state [%s] to state [%s] is impossible!', $fromState, $toState));
    }
}
