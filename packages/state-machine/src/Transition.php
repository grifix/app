<?php
declare(strict_types=1);

namespace Grifix\StateMachine;

final class Transition
{

    public function __construct(public readonly ?string $fromState, public readonly ?string $toState)
    {
    }
}
