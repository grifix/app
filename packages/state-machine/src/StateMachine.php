<?php
declare(strict_types=1);

namespace Grifix\StateMachine;

use Grifix\StateMachine\Exceptions\TransitionIsImpossibleException;

final class StateMachine
{
    /**
     * @param Transition[] $transitions
     * @throws TransitionIsImpossibleException
     */
    public function __construct(private string $state, readonly array $possibleStates, private readonly array $transitions)
    {
        $this->assertTransitionIsPossible(null, $this->state);
    }


    /**
     * @throws TransitionIsImpossibleException
     */
    public function makeTransition(string $toState): void
    {
        $this->assertTransitionIsPossible($this->state, $toState);
        $this->state = $toState;
    }

    public function __toString(): string
    {
        return $this->state;
    }

    private function assertTransitionIsPossible(?string $fromState, ?string $toState): void
    {
        if (!$this->isTransitionIsPossible($fromState, $toState)) {
            throw new TransitionIsImpossibleException($fromState, $toState);
        }
    }

    private function isTransitionIsPossible(?string $fromState, ?string $toState): bool
    {
        foreach ($this->transitions as $transition) {
            if ($transition->fromState !== $fromState) {
                continue;
            }
            if ($transition->toState !== $toState) {
                continue;
            }
            return true;
        }
        return false;
    }
}
