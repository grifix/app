<?php
declare(strict_types=1);

namespace Grifix\StateMachine\Tests;

use Grifix\StateMachine\Exceptions\TransitionIsImpossibleException;
use Grifix\StateMachine\StateMachine;
use Grifix\StateMachine\Transition;
use PHPUnit\Framework\TestCase;

final class StateMachineTest extends TestCase
{
    public function testItCreates(): void
    {
        $state = new StateMachine(
            'new',
            ['new'],
            [
                new Transition(null, 'new')
            ]
        );
        self::assertEquals('new', (string)$state);
    }

    public function testItMakesTransition(): void
    {
        $state = new StateMachine(
            'new',
            ['new', 'active'],
            [
                new Transition(null, 'new'),
                new Transition('new', 'active')
            ]
        );
        $state->makeTransition('active');
        self::assertEquals('active', (string)$state);
    }

    public function testItDoesNotMakeImpossibleTransition(): void
    {
        $state = new StateMachine(
            'new',
            ['new', 'active', 'closed'],
            [
                new Transition(null, 'new'),
                new Transition('new', 'active')
            ]
        );
        $this->expectException(TransitionIsImpossibleException::class);
        $state->makeTransition('closed');
    }
}
