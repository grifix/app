<?php
declare(strict_types=1);

namespace Grifix\Worker;

interface WorkerInterface
{
    public function run(callable $callback): void;
}
