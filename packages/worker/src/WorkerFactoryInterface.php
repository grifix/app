<?php
declare(strict_types=1);

namespace Grifix\Worker;

use Grifix\Clock\ClockInterface;
use Grifix\Memory\MemoryInterface;

interface WorkerFactoryInterface
{
    public function create(
        int  $failsLimit = 5,
        int  $memoryLimit = 100 * MemoryInterface::MB,
        int  $timeLimit = 30 * ClockInterface::MINUTE,
        int  $iterationDelay = 100000,
        bool $singleRunMode = false
    ): WorkerInterface;
}
