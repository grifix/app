<?php
declare(strict_types=1);

namespace Grifix\Worker;

use Grifix\Clock\ClockInterface;
use Grifix\Memory\MemoryInterface;
use Psr\Log\LoggerInterface;

final class WorkerFactory implements WorkerFactoryInterface
{

    public function __construct(
        private readonly ClockInterface $clock,
        private readonly MemoryInterface $memory,
        private readonly LoggerInterface $logger
    )
    {
    }

    public function create(
        int  $failsLimit = 5,
        int  $memoryLimit = 100 * MemoryInterface::MB,
        int  $timeLimit = 30 * ClockInterface::MINUTE,
        int  $iterationDelay = 100000,
        bool $singleRunMode = false
    ): WorkerInterface
    {
        return new Worker(
            $this->logger,
            $this->clock,
            $this->memory,
            $failsLimit,
            $memoryLimit,
            $timeLimit,
            $iterationDelay,
            $singleRunMode
        );
    }
}
