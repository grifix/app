<?php
declare(strict_types=1);

namespace Grifix\Worker;

use DateTimeImmutable;
use Grifix\Clock\ClockInterface;
use Grifix\Memory\MemoryInterface;
use Psr\Log\LoggerInterface;

final class Worker implements WorkerInterface
{
    private DateTimeImmutable $startDate;

    private int $failsCounter = 0;

    public function __construct(
        private readonly LoggerInterface $logger,
        private readonly ClockInterface $clock,
        private readonly MemoryInterface $memory,
        private readonly int $failsLimit = 5,
        private readonly int $memoryLimit = 100 * MemoryInterface::MB,
        private readonly int $timeLimit = 30 * ClockInterface::MINUTE,
        private readonly int $iterationDelay = 100000,
        private readonly bool $singleRunMode = false
    )
    {
    }

    public function run(callable $callback): void
    {
        $this->startDate = $this->clock->getCurrentTime();
        while (true) {
            if ($this->hasMemoryLimitExceed()) {
                return;
            }

            if ($this->hasTimeLimitExceed()) {
                return;
            }

            if ($this->hasFailsLimitExceed()) {
                return;
            }
            try {
                $callback();
                if ($this->singleRunMode) {
                    return;
                }
                usleep($this->iterationDelay);
            } catch (\Throwable $exception) {
                $this->handleFail($exception);
            }
        }
    }

    private function hasMemoryLimitExceed(): bool
    {
        if ($this->singleRunMode) {
            return false;
        }
        return $this->memory->getUsage() > $this->memoryLimit;
    }

    private function hasTimeLimitExceed(): bool
    {
        if ($this->singleRunMode) {
            return false;
        }

        return $this->clock->getCurrentTime()->getTimestamp() - $this->startDate->getTimestamp() > $this->timeLimit;
    }

    private function hasFailsLimitExceed(): bool
    {
        if ($this->singleRunMode) {
            return false;
        }
        if (null === $this->failsLimit) {
            return false;
        }

        return $this->failsCounter >= $this->failsLimit;
    }

    private function handleFail(\Throwable $throwable): void
    {
        $this->failsCounter++;
        $this->logger->error($throwable->getMessage(), [
            'file' => $throwable->getFile(),
            'line' => $throwable->getLine(),
            'code' => $throwable->getCode(),
            'trace' => $throwable->getTraceAsString(),
        ]);
    }
}
