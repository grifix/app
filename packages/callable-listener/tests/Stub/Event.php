<?php
declare(strict_types=1);

namespace Grifix\CallableListener\Tests\Stub;

final class Event
{

    public function __construct(public readonly string $value)
    {
    }
}
