<?php
declare(strict_types=1);

namespace Grifix\CallableListener\Tests;

use Grifix\CallableListener\CallableListener;
use Grifix\CallableListener\Exception\EventIsNotSupportedException;
use Grifix\CallableListener\Tests\Stub\Event;
use Grifix\CallableListener\Tests\Stub\Listener;
use Grifix\CallableListener\Tests\Stub\OtherEvent;
use PHPUnit\Framework\TestCase;

final class CallableListenerTest extends TestCase
{
    public function testItHandlesEvent(): void
    {
        $listener = new Listener();
        $callable = new CallableListener($listener);
        $callable(new Event('foo'));
        $callable(new Event('foo'));
        self::assertEquals('foo', $listener->value);
    }

    public function testItDoesNotHandleNotSupportedEvent():void{
        $callable = new CallableListener(new Listener);
        $this->expectException(EventIsNotSupportedException::class);
        $this->expectExceptionMessage('Event [Grifix\CallableListener\Tests\Stub\OtherEvent] is not supported by listener [Grifix\CallableListener\Tests\Stub\Listener]');
        $callable(new OtherEvent());
    }

    public function testIsEventSupported():void{
        $callable = new CallableListener(new Listener());
        self::assertTrue($callable->isEventSupported(Event::class));
        self::assertFalse($callable->isEventSupported(OtherEvent::class));
    }
}
