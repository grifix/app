#!/usr/bin/env bash
set -e
set -x
docker-compose down
docker volume rm -f grifix_db
docker volume rm -f grifix_test_db
docker-compose up -d --build
docker-compose exec php composer install
docker-compose exec php rm -rf var/cache
docker-compose exec php rm -rf var/test_dump.sql
docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction --env=dev
#docker-compose exec php bin/console cache:clear --env=test
#docker-compose exec php bin/console doctrine:migrations:migrate --no-interaction --env=test
